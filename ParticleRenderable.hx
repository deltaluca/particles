package;

import turbulenz.graphics.Camera;
import turbulenz.graphics.DrawParameters;
import turbulenz.graphics.GraphicsDevice;
import turbulenz.graphics.Material;
import turbulenz.graphics.Renderable;
import turbulenz.graphics.SceneNode;
import turbulenz.graphics.TechniqueParameters;
import turbulenz.MathDevice;
import turbulenz.TurbulenzEngine;

//
//
// Lazily constructed Particle Renderable.
//
// ParticleRenderable makes use of a ParticleSystem, but does not own that system.
// ParticleRenderable makes use of a set of ParticleViews that it may construct, and owns them
// unless you explicitly take them away, and will be otherwise destroyed when renderable is destroyed.
//
// Non-lazy usage:
//   renderable = new ParticleRenderable(system, ...);
//
// Lazy usage:
//   renderable = new ParticleRenderable(null, ...);
//   renderable.setExtents(center, halfExtents); // must be supplied straight away
//   renderable.lazyAssignSystem = function () { .. return system; };
//
// when renderable is being cache, can allow its system to be used elsewhere.
//   renderable.setSystem(null);
//
// Optional lazy views
//   renderable.lazyAssignView = function () { .. return view || null; };
//
// when renderable is being cached, can allow its views to be used elsewhere via lazyAssignView cb.
//   renderable.clearViews(cb : View -> Void); // clears views for use elsewhere.

#if jslib
@:expose("ParticleRenderable")
#end
@:keep
@:access(ParticleSystem)
@:access(ParticleView)
class ParticleRenderable implements Renderable {

    var md:MathDevice;
    var sys:ParticleSystem;
    var timer:Void->Float;

    var passIndex:Int;
    public function new(graphicsDevice:GraphicsDevice, mathDevice:MathDevice, sys:Null<ParticleSystem>, transparentPassIndex:Int, ?timer:Void->Float) {
        this.passIndex = transparentPassIndex;
        this.md        = mathDevice;
        this.timer     = Macros.or(defaultTimer, timer);

        renderUpdate = renderUpdateImpl;
        rendererInfo = {};
        setSystem(sys);

        if (material == null) {
            material = Material.create(graphicsDevice);
            material.meta.far         = false;
            material.meta.transparent = true;
            material.meta.decal       = false;
            material.meta.noshadows   = true;
        }
        sharedMaterial = material;
    }
    static var material:Material;
    static var defaultTimer = function () return TurbulenzEngine.time;

    public function setExtents(center, halfExtents) {
        this.center      = center;
        this.halfExtents = halfExtents;
    }

    public function setSystem(sys:ParticleSystem) {
        this.sys = sys;
        if (sys != null) {
            setExtents(sys.center, sys.halfExtents);
            drawParameters       = [sys.createRenderableDrawParameters(passIndex)];
            shadowDrawParameters = drawParameters;
        }
    }
    public function getSystem():Null<ParticleSystem>
        return this.sys;

    // Each camera has a different view on the system.
    var views:Map<Camera, ParticleView>;

    // Optional callback to enable recycling views and systems
    public var lazyAssignView:Void->Null<ParticleView> = null;
    public var lazyAssignSystem:Void->ParticleSystem = null;

    // Optionally recycle views before destroying a renderable
    public function clearViews(?recycle:ParticleView->Void) {
        if (views == null) return;
        for (camera in views.keys()) {
            if (recycle != null) {
                views[camera].setSystem(null);
                recycle(views[camera]);
            } else views[camera].destroy();
            views.remove(camera);
        }
    }
    public function destroy() {
        clearViews();
    }

    function renderUpdateImpl(camera:Camera) {
        // Update particle system if required.
        if (sys == null) setSystem(lazyAssignSystem());
        sys.sync(frameVisible, timer());

        // Update camera's view on the system.
        if (views == null) views = new Map();

        var view = views[camera];
        if (view == null) {
            if (lazyAssignView != null) view = lazyAssignView();
            if (view == null) view = new ParticleView(sys);
            else view.setSystem(sys);
            views[camera] = view;
        }

        view.modelView = md.m43Mul(node.getWorldTransform(), camera.viewMatrix, view.modelView);
        view.update(null, camera.projectionMatrix);
        drawParameters[0].setTechniqueParameters(0, sys.updateRenderParameters(view.renderParameters, view));
    }

    // RENDERABLE INTERFACE
    // ===================================================

    public function addCustomWorldExtents(extents:AABB):Void {}
    public function clone():Renderable { return null; }
    public function getCustomWorldExtents():Null<AABB> { return null; }
    public function getMaterial():Material { return null; }
    public function getWorldExtents():AABB return node.getWorldExtents();
    public function hasCustomWorldExtents():Bool { return false; }
    public function removeCustomWorldExtents():Void {}
    public function setMaterial(material:Material):Void {}
    public function getNode():Null<SceneNode> { return node; }

    public var diffuseDrawParameters:Array<DrawParameters> = null;
    public var disabled:Bool = false;
    public var drawParameters:Array<DrawParameters> = null;
    public var geometryType:String = "ParticleSystem";
    public var shadowDrawParameters:Array<DrawParameters> = null;
    public var sharedMaterial:Material = null;
    public var worldExtents:AABB = null;
    public var distance:Float = 0.0;
    public var frameVisible:Int = 0;
    public var rendererInfo:Dynamic = null;

    // ---------------------------------------
    // undocumented in turbulenz api
    //
    public var halfExtents:Null<Vector3>;
    public var center:Null<Vector3>;
    public function setNode(node:SceneNode):Void {
        this.node = node;
    }
    public var queryCounter:Int = 0;
    public var diffuseShadowDrawParameters:Array<DrawParameters> = null;
    public var shadowMappingDrawParametres:Array<DrawParameters> = null;
    public var geometry:Dynamic = null; // TODO
    public var surface:Dynamic = null; // TODO
    public var techniqueParameters:TechniqueParameters = null;
    public var skinController:Dynamic = null; // TODO
    public var isNormal:Bool = false;
    public var node:Null<SceneNode> = null;
    public var normalInfos:Dynamic = null; // TODO

    public function isSkinned():Bool { return false; }
    public var renderUpdate : Camera->Void;
}
