package;

import turbulenz.graphics.DrawParameters;
import turbulenz.graphics.GraphicsDevice;
import turbulenz.graphics.RenderTarget;
import turbulenz.graphics.Semantics;
import turbulenz.graphics.Shader;
import turbulenz.graphics.Technique;
import turbulenz.graphics.TechniqueParameters;
import turbulenz.graphics.Texture;
import turbulenz.graphics.VertexBuffer;
import turbulenz.MathDevice;
import turbulenz.TurbulenzEngine;

import js.html.Uint8Array;

typedef Rect = { x: Int, y: Int, w: Int, h: Int };
typedef RectNode = {
    rect : Rect,
    used : Bool,
    down : RectNode,
    right: RectNode
}
class RectPacker {
    // non-normalised.
    var rects:Map<Int, RectNode>;
    var root :RectNode;
    var id   :Int;

    public function new() {
        rects = new Map();
        root  = null;
        id    = 0;
    }

    public function pack(w:Int, h:Int):Int {
        var node = find(root, w, h);
        var fit = if (node != null) split(node, w, h) else grow(w, h);
        rects[id] = fit;
        return id++;
    }

    function find(root:RectNode, w, h) {
        if (root.used) {
            var a = find(root.down, w, h);
            if (a == null) a = find(root.right, w, h);
            return a;
        }
        else if (w <= root.rect.w && h <= root.rect.h)
             return root;
        else return null;
    }

    inline function node(x, y, w, h)
        return {
            rect : {x: x, y: y, w: w, h: h},
            used : false,
            down : null,
            right: null
        };

    function split(root:RectNode, w, h) {
        root.used  = true;
        root.down  = node(root.rect.x, root.rect.y + h, root.rect.w, root.rect.h - h);
        root.right = node(root.rect.x + w, root.rect.y, root.rect.w - w, h);
        return root;
    }

    function grow(w, h) {
        var canGrowDown  = w <= root.rect.w;
        var canGrowRight = h <= root.rect.h;

        // Try and keep packed size square-like
        var shouldGrowRight = canGrowRight && (root.rect.h >= (root.rect.w + w));
        var shouldGrowDown  = canGrowDown  && (root.rect.w >= (root.rect.h + h));

        return if (shouldGrowRight) growRight(w, h);
          else if (shouldGrowDown)  growDown (w, h);
          else if (canGrowRight)    growRight(w, h);
          else if (canGrowDown)     growDown (w, h);
          else                      null;
    }

    function growRight(w, h) {
        var newRoot   = node(0, 0, root.rect.w + w, root.rect.h);
        newRoot.used  = true;
        newRoot.down  = root;
        newRoot.right = node(root.rect.w, 0, w, root.rect.h);
        root = newRoot;
        var node = find(root, w, h);
        return if (node != null) split(node, w, h) else null;
    }

    function growDown(w, h) {
        var newRoot   = node(0, 0, root.rect.w, root.rect.h + h);
        newRoot.used  = true;
        newRoot.down  = node(0, root.rect.h, root.rect.w, h);
        newRoot.right = root;
        root = newRoot;
        var node = find(root, w, h);
        return if (node != null) split(node, w, h) else null;
    }
}

typedef Context = {
    textures: Array<Texture>,
    targets : Array<RenderTarget>,
    systems : Array<{
        system: ParticleSystem,
        uvRect: Vector4 // non-normalised
    }>,
    next    : Int
};

typedef SystemContext = {
    textures: Array<Texture>,
    targets:  Array<RenderTarget>,
    uvRect:   Vector4 // non-normalised
};

#if jslib
@:keep
@:expose("SharedSystemContext")
#end
class SharedSystemContext {

    var graphicsDevice:GraphicsDevice;

    var capacity = 0;
    var used     = 0;
    var systemSize:{ x: Int, y: Int };
    var contexts:Array<Context>;
    var nextContext:Int;
    var maxX:Int;
    var maxY:Int;

    static var gdMaxSize       : Int;
    static var textureVertices : VertexBuffer;
    static var textureSemantics: Semantics;
    static var copyTechnique   : Technique;
    static var copyParameters  : TechniqueParameters;

    static function init(gd:GraphicsDevice) {
        if (copyTechnique != null) return;
        textureVertices =
            gd.createVertexBuffer({
                numVertices: 4,
                attributes : [gd.VERTEXFORMAT_FLOAT2],
                _dynamic   : false,
                data       : [0,0, 1,0, 0,1, 1,1]
            });
        textureSemantics =
            gd.createSemantics([gd.SEMANTIC_POSITION]);
        copyTechnique =
            gd.createShader(Macros.compileShader('particles-copy.cgfx'))
              .getTechnique('copy');
        copyParameters = gd.createTechniqueParameters({
            src: null,
            dst: ([0,0,0,0] : turbulenz.MathDevice.Vector4)
        });
        gdMaxSize = gd.maxSupported("TEXTURE_SIZE");
    }

    function new(params:{
        graphicsDevice: GraphicsDevice,
        ?maxParticles : Int,
        ?zSorted      : Bool,
        ?capacity     : Int
    }) {
        graphicsDevice = params.graphicsDevice;
        init(graphicsDevice);

        var zSorted  = Macros.or(false, params.zSorted);
        var capacity = Macros.or(1, params.capacity);
        systemSize = ParticleSystem.computeMaxParticleDependents(params.maxParticles, zSorted).textureSize;
        // max x-y counts of systems in a single texture
        maxX = Math.floor(gdMaxSize / (systemSize.x * ParticleSystem.PARTICLE_DIM));
        maxY = Math.floor(gdMaxSize / (systemSize.y * ParticleSystem.PARTICLE_DIM));

        contexts = [];
        nextContext = 0;
        resize(capacity);
    }

    public function alloc(sys:ParticleSystem) {
        if (used >= capacity) {
            // don't resize for just 1 more space.
            // but equally, doubling the capacity could waste far too much texture space.
            // so we'll be more conservative.
            resize(Math.ceil(capacity * 1.25));
        }
        used++;
        var ctx = contexts[nextContext];
        if (ctx.next >= ctx.systems.length) {
            nextContext++;
            ctx = contexts[nextContext];
        }
        var sysCtx = ctx.systems[ctx.next];
        sysCtx.system = sys;
        ctx.next++;
        return {
            textures: ctx.textures,
            targets : ctx.targets,
            uvRect  : sysCtx.uvRect
        };
    }

    public function resize(capacity:Int) {
        if (capacity <= this.capacity)
            return;

        // num x-y counts of systems required for target capacity.
        var numX = Std.int(Math.min(maxX, capacity));
        var numY = Math.ceil(capacity / numX);
        this.capacity = numX * numY;

        var numCtx = Math.ceil(numY / maxY);
        numY %= maxY;

        // generate new (full) contexts needed.
        var newContexts = [];
        for (i in 0...(numCtx - contexts.length - 1))
            newContexts.push(createContext(graphicsDevice, maxX, maxY, systemSize));
        if (numX != 0 && numY != 0) {
            newContexts.push(createContext(graphicsDevice, numX, numY, systemSize));
        }

        // If the last old context was actually in use, then copy the data over.
        var lastCtx = if (contexts.length != 0) contexts.pop() else null;
        if (lastCtx != null && lastCtx.next != 0) {
            var newCtx = newContexts[0];
            copyTexture(graphicsDevice, lastCtx.textures[0], newCtx.targets[0]);
            copyTexture(graphicsDevice, lastCtx.textures[1], newCtx.targets[1]);
            lastCtx.targets[0].destroy();
            lastCtx.targets[1].destroy();
            lastCtx.textures[0].destroy();
            lastCtx.textures[1].destroy();
            newCtx.next = lastCtx.next;
            for (i in 0...lastCtx.next) {
                var sys = lastCtx.systems[i].system;
                newCtx.systems[i].system = sys;
                sys.updateSystemContext({
                    textures: newCtx.textures,
                    targets : newCtx.targets,
                    uvRect  : newCtx.systems[i].uvRect
                });
            }
        }
        for (c in newContexts) contexts.push(c);
    }

    static function copyTexture(gd:GraphicsDevice, from:Texture, to:RenderTarget) {
        copyParameters.src = from;
        copyParameters.dst = [
            0, 0,
            from.width / to.colorTexture0.width,
            from.height / to.colorTexture0.height
        ];

        if (!gd.beginRenderTarget(to))
            throw "oops";

        gd.setStream(textureVertices, textureSemantics);
        gd.setTechnique(copyTechnique);
        gd.setTechniqueParameters(copyParameters);
        gd.draw(gd.PRIMITIVE_TRIANGLE_STRIP, 4, 0);
        gd.endRenderTarget();
    }

    static function createContext(gd:GraphicsDevice, numX:Int, numY:Int, systemSize:{x:Int, y:Int}):Context {
        var width  = numX * systemSize.x * ParticleSystem.PARTICLE_DIM;
        var height = numY * systemSize.y * ParticleSystem.PARTICLE_DIM;
        var textures = [for (i in 0...2) gd.createTexture({
            name      : 'ParticleSystem Particles ($i)',
            width     : width,
            height    : height,
            depth     : 1,
            format    : gd.PIXELFORMAT_R8G8B8A8,
            mipmaps   : false,
            cubemap   : false,
            renderable: true,
            _dynamic  : true
        })];
        var initData = new Uint8Array(textures[0].width * textures[0].height * 4);
        textures[0].setData(initData);

        var targets = [for (i in 0...2) gd.createRenderTarget({
            colorTexture0: textures[i]
        })];

        var xScale = width / numX;
        var yScale = height / numY;
        var systems = [for (y in 0...numY) for (x in 0...numX) {
            system: null,
            uvRect: ([x * xScale, y * yScale, (x + 1) * xScale, (y + 1) * yScale] : Vector4)
        }];

        return {
            textures: textures,
            targets : targets,
            systems : systems,
            next    : 0
        };
    }

    public static function unsharedContext(gd:GraphicsDevice, systemSize:{x:Int, y:Int}):SystemContext {
        var context = createContext(gd, 1, 1, systemSize);
        return {
            textures: context.textures,
            targets : context.targets,
            uvRect  : context.systems[0].uvRect
        };
    }

    public static function destroyUnsharedContext(context:SystemContext) {
        for (t in context.textures) t.destroy();
        for (t in context.targets)  t.destroy();
    }

    public function destroy() {
        for (c in contexts) {
            for (t in c.textures) t.destroy();
            for (t in c.targets)  t.destroy();
            for (i in 0...c.next)
                c.systems[i].system.updateSystemContext(null);
        }
    }
}
