import turbulenz.*;
import turbulenz.net.*;
import turbulenz.graphics.*;
import turbulenz.util.*;

import ParticleSystem;
import ParticleBuilder;
import SharedSystemContext;

class Main {
    static function main() {
        var packer = new RectPacker();
        trace(packer.pack(100, 100));
        trace(packer.pack(200, 200));
        trace(packer.pack(10, 10));
    }
}
