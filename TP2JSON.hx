package;

class TP2JSON {
    static function main() {
        var args = Sys.args().slice(2);
        var inp = haxe.Json.parse(sys.io.File.getContent(args[0]));

        var width = inp.meta.size.w;
        var height = inp.meta.size.h;

        var frames = (inp.frames : Array<Dynamic>);
        for (f in frames) f.index = Std.parseInt(f.filename.substr(f.filename.indexOf("-")));
        frames.sort(function (a, b) return b.index - a.index);

        for (f in frames) {
            trace("[" + (f.frame.x / width) + "," + (f.frame.y / height) + "," + ((f.frame.x + f.frame.w) / width) + "," + ((f.frame.y + f.frame.h) / height) + "],");
        }
    }
}
