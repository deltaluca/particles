import ogl.DebugDraw;
import ogl.GL;
import ogl.GLM;
import glfw3.GLFW;

typedef Rect = { x: Int, y: Int, w: Int, h: Int };
typedef RectNode = {
    rect : Rect,
    used : Bool,
    children : Array<RectNode>,
    parent : Null<RectNode>
}

@:allow(Test)
class RectPacker {
    // non-normalised.
    var rects:Map<Int, Rect>;
    var root :RectNode;
    var id   :Int;

    public inline function get(i:Int) return rects[i];

    public function new() {
        rects = new Map();
        root  = null;
        id    = 0;
    }

    inline function getSize(w, h)
        return w > h ? w : h;

    public function compact(best=false) {
        // Get rects in id order.
        var rects = [ for (i in 0...id) this.rects[i] ];

        // Sort largest down to smallest.
        var processed = new Map();
        var backRef = [];
        for (i in 0...id) {
            var max = -1;
            var maxSize = 0;
            for (j in 0...id) {
                if (processed[j]) continue;
                var rect = rects[j];
                var size = getSize(rect.w, rect.h);
                if (size > maxSize || max == -1) {
                    max = j;
                    maxSize = size;
                }
            }
            backRef.push(max);
            processed[max] = true;
        }

        // Pack from scratch
        root = null;
        var lost = [];
        for (i in 0...id) {
            var rect = rects[backRef[i]];
            if (pack(rect.w, rect.h, backRef[i], best) == null)
                lost.push(backRef[i]);
        }
    }

    public function pack(w:Int, h:Int, ?forceId:Int, ?best:Bool):Null<Int> {
        if (root == null)
            root = node(0, 0, w, h);

        var node;
        if (best) {
            var nodes = [];
            findBest(root, w, h, nodes);
            nodes.sort(function (a, b) {
                var del = splitQuality(a, w, h) - splitQuality(b, w, h);
                return del < 0 ? -1 : 1;
            });
            node = nodes.length != 0 ? nodes[0] : null;
        }
        else node = find(root, w, h);

        var fit = if (node != null) split(node, w, h) else grow(w, h);
        if (fit == null) return null;
        if (forceId != null) {
            rects[forceId] = fit;
            return forceId;
        }
        else {
            rects[id] = fit;
            return id++;
        }
    }

    function splitQuality(root:RectNode, w, h) {
        var w1 = root.rect.w - w;
        var h1 = root.rect.h - h;
        return w1 * root.rect.h + h1 * root.rect.w;
    }

    function findBest(root:RectNode, w, h, out) {
        if (root.used) {
            for (c in root.children) findBest(c, w, h, out);
        }
        else if (w <= root.rect.w && h <= root.rect.h) out.push(root);
    }

    function find(root:RectNode, w, h) {
        if (root.used) {
            var a = null;
            for (c in root.children) {
                a = find(c, w, h);
                if (a != null) break;
            }
            return a;
        }
        else
            return if (w <= root.rect.w && h <= root.rect.h) root else null;
    }

    inline function node(x, y, w, h)
        return {
            rect : {x: x, y: y, w: w, h: h},
            used : false,
            children : [],
            parent : null
        };

    function split(root:RectNode, w, h) {
        root.children = [
            node(root.rect.x, root.rect.y + h, root.rect.w, root.rect.h - h),
            node(root.rect.x + w, root.rect.y, root.rect.w - w, h)
        ];
        for (c in root.children) { c.parent = root; }
        root.used = true;
        return { x: root.rect.x, y: root.rect.y, w: w, h: h };
    }

    inline function getRight(rect, w, h)
        return Math.abs(rect.h - h) > Math.abs(rect.w - w);
    function grow(w, h) {
        var canGrowRight = (root.rect.x + root.rect.w + w) <= 1024*8;
        var canGrowDown  = (root.rect.y + root.rect.h + h) <= 1024*8;

        // Try and avoid narrow regions being left.
        var sizeGrowRight = getRight(root.rect, w, h);

        return if (sizeGrowRight && canGrowRight) growRight(w, h);
          else if (canGrowDown) growDown(w, h);
          else                  null;
    }

    function growRight(w, h) {
        var newRoot;
        if (h < root.rect.h) {
            newRoot = node(root.rect.x, root.rect.y, root.rect.w + w, root.rect.h);
            newRoot.children.push(node(root.rect.x + root.rect.w, root.rect.y + h, w, root.rect.h - h));
        }
        else {
            newRoot = node(root.rect.x, root.rect.y, root.rect.w + w, h);
            newRoot.children.push(node(root.rect.x, root.rect.y + root.rect.h, root.rect.w, h - root.rect.h));
        }
        newRoot.used = true;
        newRoot.children.push(root);
        for (c in newRoot.children) c.parent = root;
        root = newRoot;
        return { x: root.rect.x + root.rect.w, y: root.rect.y, w: w, h: h };
    }

    function growDown(w, h) {
        var newRoot;
        if (w < root.rect.w) {
            newRoot = node(root.rect.x, root.rect.y, root.rect.w, root.rect.h + h);
            newRoot.children.push(node(root.rect.x + w, root.rect.y + root.rect.h, root.rect.w - w, h));
        }
        else {
            newRoot = node(root.rect.x, root.rect.y, w, root.rect.h + h);
            newRoot.children.push(node(root.rect.x + root.rect.w, root.rect.y, w - root.rect.w, root.rect.h));
        }
        newRoot.used  = true;
        newRoot.children.push(root);
        for (c in newRoot.children) c.parent = root;
        root = newRoot;
        return { x: root.rect.x, y: root.rect.y + root.rect.h, w: w, h: h };
    }
}

class Test {

    static function render(debug:DebugDraw, packer:RectPacker) {
        var renderNode = null;
        var cols = [[1.0,0,0,1.0],[1.0,0.5,0,1.0],[1.0,1.0,0,1.0],[0,1.0,0,1.0],[0,1.0,1.0,1.0],[0,0,1.0,1.0],[1.0,0,1.0,1.0]];
        for (i in 0...cols.length)
        {
            var c = cols[i];
            cols.push([c[0]/2,c[1]/2,c[2]/2,1.0]);
        }
        renderNode = function (node:RectNode, depth:Int) {
            if (node.used) {
//                if (node.rect.w != 0 && node.rect.h != 0)
//                    debug.drawBox([node.rect.x+node.rect.w/2, node.rect.y+node.rect.h/2, 0], [node.rect.w,node.rect.h,0], [1,1,1,0.025]);
            }
            else {
                if (node.rect.w != 0 && node.rect.h != 0)
                    debug.drawBox([node.rect.x+node.rect.w/2, node.rect.y+node.rect.h/2, 0], [node.rect.w,node.rect.h,0], cols[depth % cols.length]);
            }
            if (node.used)
                for (c in node.children) renderNode(c, depth+1);
        };
        if (packer.root != null)
            renderNode(packer.root, 0);
    }

    static function main() {
        GLFW.setErrorCallback(function (err:Int, msg:String) throw 'error ($err) :: $msg');
        GLFW.init();
        GLFW.windowHint(GLFW.RESIZABLE, 0);

        var window = GLFW.createWindow(1024, 1024, "Spacebar To Add: Return To Compact");
        GLFW.makeContextCurrent(window);
        GL.init();

        GL.enable(GL.BLEND);
        GL.blendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA);

        GL.viewport(0, 0, 1024, 1024);

        var packer = new RectPacker();
        var debug  = new DebugDraw();

        var dims = [for (i in 0...5) [Math.random()*20+3, Math.random()*20+3].map(Std.int)];
        for (d in dims) {
            var ave = 0.5 * (d[0] + d[1]);
            d[0] = Std.int(d[0] + (ave - d[0])*0.5);
            d[1] = Std.int(d[1] + (ave - d[1])*0.5);
        }
        for (i in 0...dims.length) {
            var d = dims[i];
            d = [d[0]*3, d[1]*3];
            dims.push(d);
        }

        GLFW.setKeyCallback(window, function (_, key:Int, state:Int, mod:Int, wtf:Int) {
            if (state == GLFW.RELEASE) return;

            var size = function (x, y) return Std.int(Math.max(x, y));
            var right = function (rect, w, h) return Math.abs(rect.h - h) > Math.abs(rect.w - w);

            if (key == GLFW.SPACE) {
                var dim = dims[Std.int(Math.random()*dims.length)];
                for (i in 0...10) {
                   packer.pack(dim[0], dim[1], null, true);
                }
            }
            else if (key == GLFW.BACKSPACE) {
                var dim = dims[Std.int(Math.random()*dims.length)];
                for (i in 0...10) {
                   packer.pack(dim[0], dim[1], null, false);
                }
            }
            else if (key == GLFW.ENTER) {
                packer = new RectPacker();
            }
            else if (key == GLFW.ONE) {
                packer.compact(true);
            }
            else
                packer.compact(false);
        });

        var projection = Mat3x2.viewportMap(1024*8, 1024*8);
        while (!GLFW.windowShouldClose(window)) {
            GLFW.waitEvents();

            if (Math.random()<0.25)
            {
                GL.clear(GL.COLOR_BUFFER_BIT);
                debug.begin();
                debug.setTransform(projection);
                render(debug, packer);
                debug.end();
                GLFW.swapBuffers(window);
            }
        }

        GLFW.destroyWindow(window);
        GLFW.terminate();
    }
}
