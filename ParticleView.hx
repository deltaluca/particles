package;

import turbulenz.graphics.RenderTarget;
import turbulenz.graphics.TechniqueParameters;
import turbulenz.graphics.Texture;
import turbulenz.MathDevice;

import js.html.Uint8Array;

#if jslib
@:keep
@:expose("ParticleView")
#end
@:allow(ParticleSystem)
@:allow(ParticleRenderable)
class ParticleView {
    // System view is upon.
    @:isVar public var system(default,null):ParticleSystem;

    // Mapping table textures/render targets.
    // Current texture/target given by current index.
    var mappingTextures:Null<Array<Texture>>;
    var mappingTargets :Null<Array<RenderTarget>>;
    var current        :Null<Int>; // 0 | 1

    // Current projection/modelView transformation matrices
    //   for rendering/z-sorting.
    var projection:Matrix44;
    var modelView :Matrix43;

    // Technique parameters for the system's renderer.
    // Each view has its own copy for different transforms
    //   permitting multiple views to be updated, and then
    //   rendered in seperate passes.
    var renderParameters:TechniqueParameters;

    // Parameters for GPU merge sort
    var mergePass  = 0;
    var mergeStage = 0;

    function new(system:ParticleSystem) {
        this.system = system;
        system.addView(this);

        if (system.zSorted) {
            mappingTextures = [for (i in 0...2) system.graphicsDevice.createTexture({
                name      : 'ParticleView Mapping ($i)',
                width     : system.particleSize.x,
                height    : system.particleSize.y,
                depth     : 1,
                format    : system.graphicsDevice.PIXELFORMAT_R8G8B8A8,
                mipmaps   : false,
                cubemap   : false,
                renderable: true,
                _dynamic  : true
            })];
            mappingTargets = [for (i in 0...2) system.graphicsDevice.createRenderTarget({
                colorTexture0: mappingTextures[i]
            })];
            current = 0;

            // Set up first mapping texture with uv-coordinates for
            // all possible particles represented in the table.
            var storageCount = system.particleSize.x * system.particleSize.y;
            var data = new Uint8Array(storageCount * 4);
            for (i in 0...storageCount) {
                var u = (i % system.particleSize.x);
                var v = Std.int((i - u) / system.particleSize.x);
                data[(i * 4) + 0] = u;
                data[(i * 4) + 1] = v;
            }
            mappingTextures[0].setData(data);
        }

        renderParameters = system.createViewRenderParameters();
    }

    // internal, used by ParticleRenderable.
    function setSystem(system:ParticleSystem) {
        if (this.system != system) {
            if (this.system != null)
                this.system.removeView(this);
            this.system = system;
            if (system != null)
                system.addView(this);
        }
    }

    function destroy() {
        if (system.zSorted) {
            for (m in mappingTextures) m.destroy();
            for (t in mappingTargets)  t.destroy();
        }
    }

    public function update(?modelView:Matrix43, ?projection:Matrix44) {
        if (modelView  != null) this.modelView  = modelView;
        if (projection != null) this.projection = projection;
        if (system.zSorted) system.sortMappingTable(this);
    }

    public function render() system.render(this);
}
