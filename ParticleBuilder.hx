package;

typedef DataT = #if (macro||build_tool) Array<Int> #else js.html.Uint32Array #end;
typedef ParticleSystemDefn = {
    maxLifeTime: Float,
    animation  : { width:Int, height:Int, data: DataT },
    particle   : Dynamic<ParticleDefn>,
    attribute  : Dynamic<AttributeRange>
};
typedef ParticleDefn = {
    lifeTime      : Float,
    animationRange: Array<Float>
};
typedef AttributeValue = Dynamic;
typedef AttributeRange = {
    min  : AttributeValue,
    delta: AttributeValue
};

typedef System = Array<Attribute>;
typedef Attribute = {
    name   : String,
    type   : AttrType,
    defv   : AttributeValue,
    defi   : Interpolator,
    min    : AttributeValue,
    max    : AttributeValue,
    storage: Storage
};
enum AttrType {
    tFloat;
    tFloat2;
    tFloat4;
    tTexture(n:Float); // later turned to int and clamped.
}
enum Storage {
    sDirect;
    sNormalized;
}

typedef Particle = {
    name       : String,
    granularity: Float,
    animation  : Array<Sequence>,
    texuvs     : Dynamic<Array<Array<Float>>>,
    texsizes   : Dynamic<Array<Float>>
};
typedef Sequence = Array<Snapshot>;
typedef Snapshot = {
    time         : Float,
    attributes   : Dynamic<AttributeValue>,
    interpolators: Dynamic<Interpolator>
};

// vs[][] -> ts[] -> t -> vs[]
typedef InterpolatorFun = Array<Array<Float>> -> Array<Float> -> Float -> Array<Float>;
typedef Interpolator = {
    fun    : InterpolatorFun,
    offsets: Array<Int>
};

@:publicFields
private class JSObj {
    static inline function hasField(x:Dynamic, f:String):Bool
        return Reflect.hasField(x, f);

    static inline function fields(x:Dynamic):Array<String>
        return Reflect.fields(x);

    // Reflect.field has to deal with cases like x being null
    // or having an accessor that can throw exceptions. Want to just have
    // raw access to a field with dynamic name.
    @:extern static inline function field(x:Dynamic, f:String):Dynamic
        return untyped x[f];

    // Reflect.setField does not return the value, which we want.
    @:extern static inline function setField(x:Dynamic, f:String, y:Dynamic):Dynamic
        return untyped x[f] = y;
}

private class Error {

    // Print strings with "" to avoid confusiong with "10.0" printed as 10.0
    public static inline function s(x:Dynamic) return if (Std.is(x, String)) '"$x"' else '$x';

    public static var errors(default,null) = "";
    public static inline function printError(x:String) {
        errors += '$x\n';
    }

    public static var warnings(default,null) = "";
    public static inline function printWarning(x:String) {
        warnings += '$x\n';
    }
    public static inline function getWarnings() {
        var ret = warnings;
        warnings = "";
        return ret;
    }

    static inline var ERROR = #if (macro||build_tool) '\x1b[31;1mERROR\x1b[0m' #else 'ERROR' #end;
    static inline var WARNING = #if (macro||build_tool) '\x1b[33;1mWARNING\x1b[0m' #else 'WARNING' #end;

    static var warningCount = 0;
    static var errorCount = 0;
    public static inline function warning(x:String) {
        printWarning('$WARNING :: $x');
        warningCount++;
    }
    public static inline function error(x:String) {
        printError('$ERROR :: $x');
        errorCount++;
    }
    public static function checkErrorState(msg:String) {
        return if (errorCount != 0) {
            var out = 'Errors ($errorCount)';
            out += ' $msg';
            printError(out);
            errorCount = 0;
            true;
        }
        else
            false;
    }
    public static function fail(msg:String) {
        printError(msg);
    }
}

// Parser System/Particle JSON definitions
// and do insular verification.
private class Parser {
    // Interpolator factories.
    static var interpolators:Dynamic<Dynamic->Interpolator>;
    static function initInterpolators() {
        if (interpolators != null) return;
        interpolators = cast {};
        interpolators.none = function (_):Interpolator return {
            offsets: [0,1],
            fun: function (xs,_,_) return xs[0]
        };
        interpolators.linear = function (_):Interpolator return {
            offsets: [0,1],
            fun: function (xs,_,t) {
                return [for (i in 0...xs[0].length) xs[0][i]*(1-t) + xs[1][i]*t];
            }
        };
        interpolators.cardinal = function (def):Interpolator return {
            offsets: [-1,0,1,2],
            fun: function (xs,ts,t) {
                var n = xs[1].length;
                var x1 = xs[1];
                var t1 = ts[1];
                var x2 = xs[2];
                var t2 = ts[2];
                // Zero the gradients at start/end points of animation
                var x0 = if (xs[0] == null) xs[1] else xs[0];
                var t0 = if (ts[0] == null) ts[1] else ts[0];
                var x3 = if (xs[3] == null) xs[2] else xs[3];
                var t3 = if (ts[3] == null) ts[2] else ts[3];
                // Gradients
                var m1 = [for (i in 0...n) (1 - def.tension) * (x2[i] - x0[i]) / (t2 - t0)];
                var m2 = [for (i in 0...n) (1 - def.tension) * (x3[i] - x1[i]) / (t3 - t1)];

                // Hermite weights
                var tsqrd = t * t;
                var tcube = tsqrd * t;
                var wx1 = 2 * tcube - 3 * tsqrd + 1;
                var wx2 = - 2 * tcube + 3 * tsqrd;
                var wm1 = tcube - 2 * tsqrd + t;
                var wm2 = tcube - tsqrd;

                return [for (i in 0...n) x1[i] * wx1 + m1[i] * wm1 + m2[i] * wm2 + x2[i] * wx2];
            }
        };
        interpolators.catmull = function (_):Interpolator return
            interpolators.cardinal({ tension: 0.0 });
    }

    // Check for any extra fields that shouldn't be present.
    static function extraFields(obj:String, x:Dynamic, excludes:Array<String>) {
        for (f in JSObj.fields(x))
            if (!Lambda.has(excludes, f))
                Error.warning('$obj has extra field \'$f\'');
    }

    // Return object field, if it does not exist, error.
    static function field<T>(obj:String, x:Dynamic, n:String):Dynamic {
        if (!JSObj.hasField(x, n)) {
            Error.error('No field \'$n\' found on $obj');
            return null;
        }
        else return JSObj.field(x, n);
    }
    // Return object field as String, if it does not exist (or is not a string), error.
    static function stringField(obj:String, x:Dynamic, n:String) {
        var ret:Dynamic = field(obj, x, n);
        if (ret != null && !Std.is(ret, String)) {
            Error.error('Field \'$n\' of $obj is not a string (${Error.s(ret)})');
            return null;
        }
        else return (ret : String);
    }
    // Return object field as Float, if it does not exist (or is not a float), error.
    static function floatField(obj:String, x:Dynamic, n:String) {
        var val:Dynamic = field(obj, x, n);
        return checkFloat(obj, n, val);
    }
    static function checkFloat(obj:String, n:String, val:Dynamic) {
        if (val != null && !Std.is(val, Float)) {
            Error.error('Field \'$n\' of $obj is not a float (${Error.s(val)})');
            return null;
        }
        else return (val : Float);
    }
    // Map object field via run function if it exists, otherwise return default result.
    public static function maybeField<R>(x:Dynamic, n:String, run:Dynamic->R, def:Void->R) {
        return if (JSObj.hasField(x, n)) run(JSObj.field(x, n)) else def();
    }
    // Check attribute value against type, and error if not compatible.
    // if acceptNull is true, then attribute (sub) values may be null.
    public static function typeAttr(obj:String, type:AttrType, acceptNull:Bool, val:Dynamic):AttributeValue {
        // Cannot perform type check with null type.
        if (type == null) return val;

        function isFloat(val:Dynamic) {
            return ((acceptNull && val==null) || Std.is(val, Float));
        }
        function checkArray(val:Dynamic, n:Int) {
            if (!Std.is(val, Array)) {
                Error.error('Value \'${Error.s(val)}\' should be a float$n for $obj');
                return null;
            }

            var val:Array<Float> = val;
            if (val.length != n)
                Error.error('Value \'${val.map(Error.s)}\' should have $n elements for float$n $obj');
            for (v in val) if (!isFloat(v))
                Error.error('Element of value \'${val.map(Error.s)}\' should be a float (${Error.s(v)}) for $obj');
            return val;
        }
        return switch(type) {
        case tFloat | tTexture(_):
            if (!isFloat(val)) {
                Error.error('Value \'${Error.s(val)}\' should be a float for $obj');
                null;
            }
            else val;
        case tFloat2: checkArray(val, 2);
        case tFloat4: checkArray(val, 4);
        };
    }
    // Return a default attribute for given type and (sub) value.
    public static function defaultAttr<T>(type:AttrType, x:T=null):AttributeValue {
        // can't type check for null type
        if (type == null) return null;

        return switch(type) {
        case tFloat | tTexture(_): x;
        case tFloat2: [x,x];
        case tFloat4: [x,x,x,x];
        };
    }

    // Parse a System from JSON data.
    public static function parseSystem(def:Dynamic):System {
        var attrs = if (!Std.is(def, Array)) {
            Error.error('system definition must be an array');
            null;
        }
        else {
            var attrs = (def : Array<Dynamic>).map(parseSystemAttribute);
            // check for duplicates
            for (i in 0...attrs.length) for (j in (i+1)...attrs.length)
                if (attrs[i].name == attrs[j].name)
                    Error.error('system definition cannot have equally named attributes (${attrs[i].name})');
            attrs;
        }
        return if (Error.checkErrorState("System parse failed!")) null else attrs;
    }
    // Parse a System Attribute from JSON data.
    static function parseSystemAttribute(def:Dynamic):Attribute {
        initInterpolators();

        var name = stringField('system attribute', def, "name");
        if (name != null && name.length > 14 && name.substr(name.length-14) == "-interpolation") {
            Error.error('system attribute cannot have \'-interpolation\' as a suffix ($name)');
            name = null;
        }
        var printName  = if (name == null) '' else ' \'$name\'';
        var printNames = if (name == null) '\'s' else ' \'$name\'s';

        var stringField = stringField.bind('system attribute$printName');
        var parseInterpolator =
            parseInterpolator.bind('system attribute$printNames default-interpolation field');

        var type = stringField(def, "type");
        var type = if (type != null) switch (type) {
            case "float":  tFloat;
            case "float2": tFloat2;
            case "float4": tFloat4;
            case t:
                if (t.substr(0,7) == "texture") tTexture(Std.parseFloat(t.substr(7)));
                else {
                    Error.error('Unknown attribute type \'$t\' for system attribute$printName');
                    null;
                }
        } else null;
        var typeAttr = function (n) return typeAttr.bind('system attribute$printNames $n field', type);

        var defv = maybeField(def, "default", typeAttr("default").bind(false),
                                defaultAttr.bind(type, 0));
        var defi = maybeField(def, "default-interpolation", parseInterpolator,
                                interpolators.linear.bind(null));

        function parseMinMax(n) {
            // can't type check for null type.
            if (type == null) return null;

            return switch (type) {
            case tFloat | tFloat2 | tFloat4:
                maybeField(def, n, typeAttr(n).bind(true), defaultAttr.bind(type, null));
            case _:
                if (JSObj.hasField(def, n))
                    Error.error('$n is not accepted for system texture attribute$printName');
                null;
            }
        };
        var min = parseMinMax("min");
        var max = parseMinMax("max");

        // can't type check for null type.
        var storage = if (type == null) null else switch (type) {
            case tFloat | tFloat2 | tFloat4: maybeField(def, "storage",
                function (v) return switch (v) {
                case "direct": sDirect;
                case "normalized": sNormalized;
                case _:
                    Error.error('Unknown storage type \'$v\' for system attribute$printName');
                    null;
                },
                function () return sNormalized);
            case _:
                if (JSObj.hasField(def, "storage"))
                    Error.error('storage type is not accepted for system texture attribute$printName');
                null;
        };

        extraFields('system attribute$printName', def, ["name", "type", "default", "default-interpolation", "min", "max", "storage"]);

        return {
            name: name,
            type: type,
            defv: defv,
            defi: defi,
            min: min,
            max: max,
            storage: storage
        };
    }
    static function parseInterpolator(obj:String, def:Dynamic) {
        initInterpolators();
        return if (Std.is(def, String)) switch ((def:String)) {
            case "none": interpolators.none(null);
            case "linear": interpolators.linear(null);
            case "catmull": interpolators.catmull(null);
            case t:
                Error.error('Unknown interpolator type \'$t\' for $obj');
                null;
        } else if (def == null) {
            Error.error('Interpolator cannot be null for $obj');
            null;
        }
        else {
            var type = stringField(obj, def, "type");
            if (type == null) null;
            else switch (type) {
            case "none":
                extraFields(obj, def, ["type"]);
                interpolators.none(null);
            case "linear":
                extraFields(obj, def, ["type"]);
                interpolators.linear(null);
            case "catmull":
                extraFields(obj, def, ["type"]);
                interpolators.catmull(null);
            case "cardinal":
                extraFields(obj, def, ["type", "tension"]);
                var tension = floatField(obj, def, "tension");
                return interpolators.cardinal({tension:tension});
            case t:
                Error.error('Unknown complex interpolator type \'$t\' for $obj');
                null;
            }
        }
    }

    static function parseAttributeValue(obj:String, def:Dynamic):AttributeValue {
        if (def == null) {
            Error.error('$obj cannot be null');
            return null;
        }
        if (Std.is(def, Float)) return (def : Float);
        if (Std.is(def, Array)) {
            // at this point, we assume we have a tFloat2 or tFloat4
            // as no interpolator is defined by an array.
            var def = (def : Array<Float>);
            for (d in def) if (!Std.is(d, Float)) {
                Error.error('Element of $obj has none float value (${Error.s(d)})');
                return null;
            }
            if (def.length != 2 && def.length != 4) {
                Error.error('$obj should have either 2 or 4 elements for float2/float4 value');
                return null;
            }
            return def;
        }
        Error.error('$obj has unrecognised value type');
        return null;
    }

    public static function parseParticle(def:Dynamic):Particle {
        initInterpolators();

        var name = stringField('particle', def, "name");
        var printName = if (name == null) '' else ' \'$name\'';
        var printNames = if (name == null) '\'s' else ' \'$name\'s';

        var stringField = stringField.bind('particle$printName');
        var floatField = floatField.bind('particle$printName');

        var granularity = maybeField(def, "granularity", checkFloat.bind('particle$printName', 'granularity'),
                                function () return 1.0/60.0);
        if (granularity != null && granularity <= 0) {
            Error.error('particle$printNames granularity ($granularity) must be > 0');
            granularity = null;
        }

        var textures = [for (f in JSObj.fields(def)) if (f.substr(0,7) == "texture" && f.substr(f.length-5) != "-size") f];
        var texuvs   = cast {};
        var texsizes = cast {};
        for (f in textures) {
            var uvs = JSObj.field(def, f);
            if (!Std.is(uvs, Array)) {
                Error.error('particle$printNames $f should be an array');
            }
            else {
                uvs = (uvs : Array<AttributeValue>).map(function (uv) {
                    var uv = (uv : Array<Float>).copy();
                    return typeAttr('element of particle$printNames $f', tFloat4, false, uv);
                });
                JSObj.setField(texuvs, f, uvs);
            }
            if (JSObj.hasField(def, '$f-size')) {
                var size = JSObj.field(def, '$f-size');
                size = typeAttr('particle$printNames $f-size', tFloat2, false, size);
                JSObj.setField(texsizes, f, size);
            }
        }

        var animation = field('particle$printName', def, 'animation');
        if (animation != null && !Std.is(animation, Array)) {
            Error.error('particle$printNames animation must be an array');
            animation = null;
        }
        var animation = (animation : Array<Dynamic>);
        var animation = if (animation != null) animation.map(function (seq):Sequence {
            return if (!Std.is(seq, Array)) {
                Error.error('particle$printNames animation sequence must be an array');
                null;
            }
            else {
                var seq = (seq : Array<Dynamic>);
                seq.map(function (snap):Snapshot {
                    var obj = 'particle$printNames animation sequence snapshot';
                    if (snap == null) {
                        Error.error('$obj cannot be null');
                        return null;
                    }

                    var time;
                    if (snap == seq[0]) {
                        time = maybeField(snap, 'time', checkFloat.bind(obj, 'time'), function () return 0);
                        if (time != 0) {
                            Error.error('particle$printName animation sequence first snapshot cannot have time != 0');
                            time = null;
                        }
                    }
                    else {
                        time = Parser.floatField(obj, snap, 'time');
                        if (time != null && time <= 0) {
                            Error.error('$obj time must be positive');
                            time = null;
                        }
                    }

                    var attributes = cast {};
                    var interpolators = cast {};
                    for (f in JSObj.fields(snap)) {
                        if (f == "time") continue;
                        if (f.length > 14 && f.substr(f.length-14) == "-interpolation") {
                            var attr = f.substr(0, f.length-14);
                            JSObj.setField(interpolators, attr,
                                parseInterpolator('$obj attribute \'$attr\'', JSObj.field(snap, f)));
                        }
                        else JSObj.setField(attributes, f,
                                parseAttributeValue('$obj attribute \'$f\'', JSObj.field(snap, f)));
                    }

                    return {
                        time         : time,
                        attributes   : attributes,
                        interpolators: interpolators
                    };
                });
            }
        }) else null;

        extraFields('particle$printName', def, textures.concat(["name", "granularity", "animation"]));

        return if (Error.checkErrorState('Particle$printName parse failed!')) null
        else {
            name       : name,
            granularity: granularity,
            animation  : animation,
            texuvs     : texuvs,
            texsizes   : texsizes
        };
    }
}

private class Types {

    // try and unify types
    public static inline function unify(msg:String, a:AttrType, b:AttrType) {
        if (a != null && b != null && !Type.enumEq(a,b))
            switch([a,b]) {
                case [tTexture(_), tFloat4]:
                case [tFloat4, tTexture(_)]:
                case _: Error.error(msg);
            };
    }

    // try and assign type to given value
    public static inline function checkTypeAssign(objx:String, objt:String, x:AttributeValue, t:AttrType) {
        if (t != null) switch (t) {
            case tFloat: if (!Std.is(x, Float))
                Error.error('Cannot type ${Error.s(x)} with type float for $objt in $objx');
            case tFloat2: if (!Std.is(x, Array) || (x : Array<Float>).length != 2)
                Error.error('Cannot type ${Error.s(x)} with type float2 for $objt in $objx');
            case tFloat4: if (!Std.is(x, Array) || (x : Array<Float>).length != 4)
                Error.error('Cannot type ${Error.s(x)} with type float4 for $objt in $objx');
            case tTexture(n): if (!Std.is(x, Float))
                Error.error('Cannot type ${Error.s(x)} with type texture$n for $objt in $objx');
        };
    }
}

typedef TextureMapping = Array<Array<Float>>;
typedef AnimationTweak = Dynamic<AttributeValue>;

#if turbulenz
typedef CombinedTexture = {
    texture: turbulenz.graphics.Texture,
    mapping: TextureMapping
};
#end

@:expose("ParticleBuilder")
@:keep
class ParticleBuilder {

#if turbulenz
    public static function buildAnimationTexture(gd:turbulenz.graphics.GraphicsDevice, animation:{width:Int, height:Int, data:DataT}):turbulenz.graphics.Texture {
        return gd.createTexture({
            name: "ParticleSystemAnimationTexture",
            width: animation.width,
            height: animation.height,
            depth: 1,
            format: gd.PIXELFORMAT_R8G8B8A8,
            mipmaps: false,
            cubemap: false,
            renderable: false,
            _dynamic: false,
            data: new js.html.Uint8Array(animation.data.buffer)
        });
    }

    static inline function nearPow2Geq(x:Float)
        return 1 << Math.ceil(Math.log(x) / Math.log(2));

    static var packedInit = false;
    static var packedTextureVertices :turbulenz.graphics.VertexBuffer;
    static var packedTextureSemantics:turbulenz.graphics.Semantics;
    static var packedCopyTechnique   :turbulenz.graphics.Technique;
    static var packedCopyParameters  :turbulenz.graphics.TechniqueParameters;
    // Takes array of textures, one for each particle in a system
    //     and returns a list of uv-rectangles in the packed texture, one for each particle.
    // This Does not duplicate shared textures.
    // Textures are shrunk by in width/height to maintain a border around textures
    // (with emulated border-clamp) to prevent leaking during sampling and without
    // introducing a gap since the general use case is to provide a set of already power of 2
    // textures, and introducing a gap would mean a lot of wasted space.
    public static function combineTextures(
        gd      : turbulenz.graphics.GraphicsDevice,
        textures: Array<turbulenz.graphics.Texture>,
        border  : Int = 2
    ):CombinedTexture {
        var unique = [];
        var mapping:Dynamic = cast {};
        for (i in 0...textures.length) {
            var t = textures[i];
            if (JSObj.hasField(mapping, t.name))
                (JSObj.field(mapping, t.name) : { indices: Array<Int> }).indices.push(i);
            else {
                JSObj.setField(mapping, t.name, { map: null, indices: [i] });
                unique.push(t);
            }
        }

        var packed = Packer.pack([for (t in unique) { w: t.width, h: t.height }]);
        var w = nearPow2Geq(packed.size.w);
        var h = nearPow2Geq(packed.size.h);

        for (i in 0...unique.length) {
            var x = packed.packing[i].x / w;
            var y = packed.packing[i].y / h;
            (JSObj.field(mapping, unique[i].name) : { map: Array<Float> }).map = [
                x, y,
                x + (unique[i].width  / w),
                y + (unique[i].height / h)
            ];
        }

        var texture = gd.createTexture({
            name      : "ParticleBuilder Packed Texture",
            width     : w,
            height    : h,
            depth     : 1,
            format    : gd.PIXELFORMAT_R8G8B8A8,
            mipmaps   : true,
            cubemap   : false,
            renderable: true,
            _dynamic  : false,
            data      : [for (i in 0...w*h*4) 0x00]
        });
        var target = gd.createRenderTarget({
            colorTexture0: texture
        });

        var textureVertices, textureSemantics, params;
        if (!packedInit) {
            textureVertices = packedTextureVertices =
                gd.createVertexBuffer({
                    numVertices: 4,
                    attributes : [gd.VERTEXFORMAT_FLOAT2],
                    _dynamic   : false,
                    data       : [0,0, 1,0, 0,1, 1,1]
                });
            textureSemantics = packedTextureSemantics =
                gd.createSemantics([gd.SEMANTIC_POSITION]);
            params = packedCopyParameters =
                gd.createTechniqueParameters({
                    src   : null,
                    border: border,
                    dim   : ([0,0] : turbulenz.MathDevice.Vector2),
                    dst   : ([0,0,0,0] : turbulenz.MathDevice.Vector4)
                });
            packedCopyTechnique =
                gd.createShader(Macros.compileShader('particles-packer.cgfx'))
                  .getTechnique('pack');
        }
        else {
            textureVertices  = packedTextureVertices;
            textureSemantics = packedTextureSemantics;
            params           = packedCopyParameters;
        }

        if (!gd.beginRenderTarget(target))
            return null;
        gd.setStream(textureVertices, textureSemantics);
        gd.setTechnique(packedCopyTechnique);
        for (t in unique) {
            params.src = t;
            params.dim = [t.width, t.height];
            params.dst = (JSObj.field(mapping, t.name) : { map: Array<Float> }).map;
            gd.setTechniqueParameters(params);
            gd.draw(gd.PRIMITIVE_TRIANGLE_STRIP, 4, 0);
        }
        gd.endRenderTarget();

        var maps = [for (i in 0...textures.length) {
            var map = (JSObj.field(mapping, textures[i].name) : { map: Array<Float> }).map;
            // remap uv-rectangle to take into account introduced borders.
            [map[0] + border/w, map[1] + border/h, map[2] - border/w, map[3] - border/h];
        }];

        return {
            texture: texture,
            mapping: maps
        };
    }
#end

    // Node entry point
#if build_tool
    public static function main() {
        // minimalist node externs
        var fs = untyped __js__("require('fs')");
        var JSONparse = untyped __js__("JSON.parse");
        var JSONstringify = untyped __js__("JSON.stringify");
        var processExit = untyped __js__("process.exit");
        var args:Array<String> = untyped __js__("process.argv").slice(2);

        if (args.length < 3) {
            Error.error('Invalid arguments');
            Error.error('Arguments: -o name [-s system.json] [particle.json]+');
            processExit(1);
        }

        // Parse arguments
        var out = null;
        var system = null;
        var particles = [];
        var i = 0; while (i < args.length) {
            var arg = args[i++];
            switch (arg) {
            case '-o':
                if (i+1 == args.length) {
                    Error.error('-o expects identifier to follow');
                    processExit(1);
                }
                out = args[i++];
            case '-s':
                if (i+1 == args.length) {
                    Error.error('-s expects json path to follow');
                    processExit(1);
                }
                system = args[i++];
            case t:
                particles.push(t);
            }
        }

        // compile json definitions
        var defn = compile(
            particles.map(fs.readFileSync).map(JSONparse),
            system == null ? null : JSONparse(fs.readFileSync(system))
        );
        if (defn == null) processExit(1);

        // output json definition
        fs.writeFileSync('$out.json', JSONstringify({
            maxLifeTime: defn.maxLifeTime,
            attribute: defn.attribute,
            particle: defn.particle
        }));

        // output image png
        var data = defn.animation;
        var png = untyped __js__("new (require('pngjs')).PNG({width: data.width, height: data.height})");
        for (i in 0...data.width*data.height*4) png.data[i] = data.data[i];
        png.pack().pipe(fs.createWriteStream('$out.png'));
    }
#end

    public static function systemAttributes(?system:Dynamic):System {
        if (system == null) system = Macros.parseJSON("default-system");
        var sys = Parser.parseSystem(system);
        return sys;
    }

    public static function compile(
        particles:Array<Dynamic>,
        ?system  :Dynamic,
        ?mapping :TextureMapping,
        ?tweaks  :Array<AnimationTweak>
    ):ParticleSystemDefn {
        if (system == null) system = Macros.parseJSON("default-system");
        var sys = Parser.parseSystem(system);
        var parts = particles.map(Parser.parseParticle);

        // Can't go any further
        if (sys == null) {
            Error.fail('Build failed!');
            return null;
        }

        // Normalize texture UVs if required
        for (p in parts) {
            if (p == null) continue;
            for (f in JSObj.fields(p.texuvs)) {
                if (!JSObj.hasField(p.texsizes, f)) continue;
                var uvs  = (JSObj.field(p.texuvs, f) : Array<Array<Float>>);
                var size = (JSObj.field(p.texsizes, f) : Array<Float>);
                for (uv in uvs) {
                    uv[0] /= size[0];
                    uv[1] /= size[1];
                    uv[2] /= size[0];
                    uv[3] /= size[1];
                }
            }
        }

        // Check particles against system definitions
        // And assign any texture-uvs that were not defined to [[0,0,1,1]]
        for (p in parts) if (p != null) checkAttributes(sys, p);

        // Can't go any further
        if (Error.checkErrorState('Build failed!'))
            throw Error.errors;
        if (Lambda.has(parts, null)) {
            Error.fail('Build failed!');
            throw Error.errors;
        }

        for (p in parts) {
            flatten(sys, p);
            discretise(sys, p);
        }

        // Remap texture UVs for packed textures
        if (mapping != null) {
            for (i in 0...parts.length) {
                var p = parts[i];
                var map = mapping[i];
                // TODO: future, handle where we have more than just texture0 being used
                //       in a system, and where we would have 1 map for each texture index.
                for (tex in JSObj.fields(p.texuvs)) {
                    for (uv in (JSObj.field(p.texuvs, tex) : Array<Array<Float>>)) {
                        uv[0] = map[0] + uv[0] * (map[2] - map[0]);
                        uv[1] = map[1] + uv[1] * (map[3] - map[1]);
                        uv[2] = map[0] + uv[2] * (map[2] - map[0]);
                        uv[3] = map[1] + uv[3] * (map[3] - map[1]);
                    }
                }
            }
        }

        if (tweaks != null) {
            // TODO
            //      check for extra fields.
            for (attr in sys) {
                var typeAttr = function (n) return Parser.typeAttr.bind('tweak field', attr.type);
                var scaleName  = '${attr.name}Scale';
                var offsetName = '${attr.name}Offset';
                for (i in 0...parts.length) {
                    var p = parts[i];
                    var tweak = tweaks[i];

                    var scale  = Parser.maybeField(tweak, scaleName,
                                                   typeAttr(scaleName).bind(false),
                                                   Parser.defaultAttr.bind(attr.type, 1));
                    var offset = Parser.maybeField(tweak, offsetName,
                                                   typeAttr(offsetName).bind(false),
                                                   Parser.defaultAttr.bind(attr.type, 0));

                    for (seq in p.animation) for (snap in seq) {
                        JSObj.setField(snap.attributes, attr.name,
                            offsetScaleValue(attr.type,
                                             JSObj.field(snap.attributes, attr.name),
                                             scale, offset));
                    }
                }
            }
        }

        for (p in parts) {
            clampAttributes(sys, p);
        }

        var minmax = attributesMapping(sys, parts);
        normalizeAttributes(sys, parts, minmax);

        var width = 0;
        for (p in parts) width += p.animation[0].length;
        var data = compileData(sys, width, parts);

        var pdefns:Dynamic<ParticleDefn> = cast {};
        var prev = 0;
        for (p in parts) {
            var snaps = p.animation[0];
            JSObj.setField(pdefns, p.name, {
                lifeTime: snaps[snaps.length-1].time,
                animationRange: [prev/width, (prev = prev + snaps.length)/width]
            });
        }

        if (Error.checkErrorState('Build failed!'))
            throw Error.errors;
        if (Lambda.has(parts, null)) {
            Error.fail('Build failed!');
            throw Error.errors;
        }

        var maxLifeTime = 0;
        for (p in parts) {
            var time = JSObj.field(pdefns, p.name).lifeTime;
            if (time > maxLifeTime) maxLifeTime = time;
        }

        return {
            maxLifeTime: maxLifeTime,
            animation: { width: width, height: sys.length, data: data },
            particle: pdefns,
            attribute: minmax
        };
    }

    static function getWarnings() {
        return Error.getWarnings();
    }

    // compile final processed animations into texture (Array<Int32> RGBA data)
    static function compileData(system:System, width:Int, particles:Array<Particle>):DataT {
        var count = system.length * width;
        var data = #if (macro||build_tool) [] #else new js.html.Uint32Array(count) #end;
        var i = 0;
        function write(x:Int) {
            data[i++] = x;
        }
        for (attr in system) {
            for (p in particles) for (snap in p.animation[0]) {
                var value:AttributeValue = JSObj.field(snap.attributes, attr.name);
                switch (attr.type) {
                case tFloat:  write(Encode.encodeUnsignedFloat((value : Float)));
                case tFloat2: write(Encode.encodeUnsignedFloat2((value : Array<Float>)));
                case tFloat4: write(Encode.encodeUnsignedFloat4((value : Array<Float>)));
                case tTexture(n):
                    var uvs:Array<Array<Float>> = JSObj.field(p.texuvs, 'texture$n');
                    var ind = Std.int((value : Float));
                    if (ind < 0) ind = 0; if (ind >= uvs.length) ind = uvs.length - 1;
                    write(Encode.encodeUnsignedFloat4(uvs[ind]));
                }
            }
        }
        return data;
    }

    static function offsetScaleValue(type:AttrType, value:AttributeValue, scale:AttributeValue, offset:AttributeValue):AttributeValue {
        return switch (type) {
            case tFloat:      (value : Float) * (scale : Float) + (offset : Float);
            case tTexture(_): (value : Float) * (scale : Float) + (offset : Float);
            case tFloat2 | tFloat4:
                var value  = (value  : Array<Float>);
                var scale  = (scale  : Array<Float>);
                var offset = (offset : Array<Float>);
                [ for (i in 0...value.length) value[i] * scale[i] + offset[i] ];
        };
    }

    // Flatten particle animation into a single sequence
    // performing necessary interpolations for mid-snapshot attributes
    // and checking as we go that there is no ambiguity in the particle.
    static function flatten(system:System, particle:Particle) {
        // compute absolute time for each snapshot
        for (seq in particle.animation) {
            var pre = 0.0;
            for (snap in seq) pre = (snap.time += pre);
        }

        // order snapshots collectively by time (descending)
        var snaps = [];
        for (seq in particle.animation) snaps = snaps.concat(seq);
        snaps.sort(function (x, y) return x.time > y.time ? -1 : x.time < y.time ? 1 : 0);

        // merge equal time snapshots (ascending)
        var merged = [];
        while (snaps.length > 0) {
            var ss = [snaps.pop()];
            while (snaps.length > 0) {
                var s = snaps[snaps.length - 1];
                if (s.time == ss[0].time) ss.push(snaps.pop());
                else break;
            }
            merged.push(merge(system, particle, ss));
        }

        // copy forwards any missing interpolators.
        for (i in 1...merged.length) {
            var curr = merged[i];
            var prev = merged[i-1];
            for (attr in system) {
                // copy interpolator from previous snapshot if not defined.
                if (JSObj.hasField(curr.interpolators, attr.name)) continue;
                JSObj.setField(curr.interpolators, attr.name, JSObj.field(prev.interpolators, attr.name));
            }
        }

        // fill in any attribute values that don't need interpolation
        // (x -> y -> _ -> _ -> z -> _ -> _ -> _
        // regardless of interpolators, the 3 _ after z can only ever have
        // the value of z and we can skip interpolation
        for (attr in system) {
            var max = 0;
            for (i in 1...merged.length)
                if (JSObj.hasField(merged[i].attributes, attr.name)) max = i;
            for (i in (max+1)...merged.length)
                JSObj.setField(merged[i].attributes, attr.name, JSObj.field(merged[max].attributes, attr.name));
        }

        // interpolate any missing attribute values.
        for (i in 1...merged.length) {
            var curr = merged[i];
            var prev = merged[i-1];
            for (attr in system) {
                if (JSObj.hasField(curr.attributes, attr.name)) continue;
                // search for snapshots backwards/forwards in time defining attribute
                var back = [for (j in 0...i) if (JSObj.hasField(merged[j].attributes, attr.name)) merged[j]];
                var forth = [for (j in (i+1)...merged.length) if (JSObj.hasField(merged[j].attributes, attr.name)) merged[j]];
                // due to previous steps there is always at least one back, and one forth snapshot
                // so we can safely compute a time ratio
                var interpolator = JSObj.field(prev.interpolators, attr.name);
                var t = (curr.time - back[back.length - 1].time) / (forth[0].time - back[back.length - 1].time);
                var snaps = back.concat(forth);
                JSObj.setField(curr.attributes, attr.name, interp(interpolator, snaps, i, attr, t));
            }
        }

        particle.animation = [merged];
    }

    static function discretise(system:System, particle:Particle) {
        var disc = [];
        var snaps = particle.animation[0];
        if (snaps.length == 1) disc = snaps;
        else {
            var time = 0.0;
            var prev = snaps[0];
            var curr = snaps[1]; var curi = 1;
            while (true) {
                var t = (time - prev.time) / (curr.time - prev.time);
                var chunk:Snapshot = {
                    time: time,
                    attributes: cast {},
                    interpolators: null
                };
                for (attr in system)
                    JSObj.setField(chunk.attributes, attr.name,
                        interp(JSObj.field(prev.interpolators, attr.name), snaps, curi, attr, t));
                disc.push(chunk);
                time += particle.granularity;
                if (time >= curr.time) {
                    prev = curr;
                    curi++;
                    if (curi >= snaps.length) break;
                    curr = snaps[curi];
                }
            }
            // depending on granularity, may have missed last snapshot.
            if (disc[disc.length-1].time < snaps[snaps.length-1].time)
                disc.push(snaps[snaps.length-1]);
        }
        particle.animation = [disc];
    }

    static function clampAttributes(system:System, particle:Particle) {
        for (snap in particle.animation[0]) {
            for (attr in system)
                JSObj.setField(snap.attributes, attr.name,
                    clamp(attr, JSObj.field(snap.attributes, attr.name)));
        }
    }

    // Clamp attribute value agaisnt min/max values
    static function clamp(attr:Attribute, value:AttributeValue):AttributeValue {
        function clamp(x:Float, min:Null<Float>, max:Null<Float>)
            return if (min != null && x < min) min else if (max != null && x > max) max else x;
        return switch (attr.type) {
            case tFloat: clamp(value, attr.min, attr.max);
            case tFloat2 | tFloat4:
                var value = (value : Array<Float>);
                var max = (attr.max : Array<Float>);
                var min = (attr.min : Array<Float>);
                [for (i in 0...value.length) clamp(value[i], min[i], max[i])];
            case tTexture(_): value; // clamped on compile.
        }
    }

    static function attributesMapping(system:System, particles:Array<Particle>) {
        var res:Dynamic<AttributeRange> = cast {};
        var inf = Math.POSITIVE_INFINITY;
        for (attr in system) {
            if (attr.storage == null || Type.enumEq(attr.storage, sDirect)) continue;
            var min:AttributeValue = null;
            var max:AttributeValue = null;
            switch (attr.type) {
            case tFloat:  min = inf; max = -inf;
            case tFloat2 | tFloat4: var n = attr.defv.length;
                min = [for (i in 0...n) inf];
                max = [for (i in 0...n) -inf];
            case _:
            }
            for (p in particles) for (snap in p.animation[0]) {
                var value:AttributeValue = JSObj.field(snap.attributes, attr.name);
                switch (attr.type) {
                case tFloat:
                    var value = (value : Float);
                    if (value < (min : Float)) min = value;
                    if (value > (max : Float)) max = value;
                case tFloat2 | tFloat4:
                    var value = (value : Array<Float>);
                    var min = (min : Array<Float>);
                    var max = (max : Array<Float>);
                    for (i in 0...value.length) {
                        if (value[i] < min[i]) min[i] = value[i];
                        if (value[i] > max[i]) max[i] = value[i];
                    }
                case _:
                }
            }
            switch (attr.type) {
            case tFloat: JSObj.setField(res, attr.name, { min: min, delta: (max : Float) - (min : Float) });
            case tFloat2 | tFloat4: var n = attr.defv.length;
                var min = (min : Array<Float>);
                var max = (max : Array<Float>);
                JSObj.setField(res, attr.name, { min: min, delta: [for (i in 0...n) max[i] - min[i]] });
            case _:
            }
        }
        return res;
    }

    static function normalizeAttributes(system:System, particles:Array<Particle>, mm:Dynamic<AttributeRange>) {
        for (attr in system) {
            if (attr.storage == null || Type.enumEq(attr.storage, sDirect)) continue;
            var mm:AttributeRange = JSObj.field(mm, attr.name);
            for (p in particles) for (snap in p.animation[0]) {
                var value:AttributeValue = JSObj.field(snap.attributes, attr.name);
                JSObj.setField(snap.attributes, attr.name, switch (attr.type) {
                    case tFloat:
                        var min = (mm.min : Float);
                        var delta = (mm.delta : Float);
                        (((value : Float) - min) * (if (delta != 0) 1 / delta else 1) : AttributeValue);
                    case tFloat2 | tFloat4:
                        var value = (value : Array<Float>);
                        var min = (mm.min : Array<Float>);
                        var delta = (mm.delta : Array<Float>);
                        ([for (i in 0...value.length)
                            (value[i] - min[i]) * (if (delta[i] != 0) 1/delta[i] else 1 )
                        ] : AttributeValue);
                    case _: value;
                });
            }
        }
    }

    // Interpolate between given snapshots for attribute and given time ratio
    static function interp(intp:Interpolator, snaps:Array<Snapshot>, i:Int, attr:Attribute, t:Float):AttributeValue {
        i -= 1;
        var wrap = switch (attr.type) {
            case tFloat | tTexture(_): true;
            case _: false;
        };
        var ts = [for (x in intp.offsets)
            if (x + i < 0 || x + i >= snaps.length) null
            else snaps[x + i].time];
        var xs = [for (x in intp.offsets)
            if (x + i < 0 || x + i >= snaps.length) null
            else {
                var f = JSObj.field(snaps[x + i].attributes, attr.name);
                if (wrap) untyped [f] else untyped f;
            }];
        var val = intp.fun(xs, ts, t);
        return switch (attr.type) {
            case tFloat | tTexture(_): return val[0];
            case tFloat2 | tFloat4: return val;
        };
    }

    // Merge equal (absolute) time snapshots.
    // System provides default values for missing attributes when merging intial t=0 snapshots.
    // Otherwise are left blank for later interpolation.
    static function merge(system:System, particle:Particle, snaps:Array<Snapshot>):Snapshot {
        if (snaps.length == 1 && snaps[0].time != 0) return snaps[0];
        var merged = {
            time: snaps[0].time,
            attributes: cast {},
            interpolators: cast {}
        };
        for (attr in system) {
            // merge attribute values
            var vals = [for (snap in snaps)
                if (JSObj.hasField(snap.attributes, attr.name)) JSObj.field(snap.attributes, attr.name)
            ];
            if (vals.length > 1)
                Error.error('Merging snapshots at time=${snaps[0].time}, multiple values found for attribute \'${attr.name}\'');
            if (vals.length != 0 || snaps[0].time == 0)
                JSObj.setField(merged.attributes, attr.name,
                    if (vals.length == 0) attr.defv
                    else vals[0]);

            // merge attribute interpolators
            var vals = [for (snap in snaps)
                if (JSObj.hasField(snap.interpolators, attr.name)) JSObj.field(snap.interpolators, attr.name)
            ];
            if (vals.length > 1)
                Error.error('Merging snapshots at time=${snaps[0].time}, multiple values found for attribute interpolator\'${attr.name}\'');
            if (vals.length != 0 || snaps[0].time == 0)
                JSObj.setField(merged.interpolators, attr.name,
                    if (vals.length == 0) attr.defi
                    else vals[0]);
        }
        return merged;
    }

    // verify that the particle uses attributes only defined by the system
    // and that types are equal.
    static function checkAttributes(system:System, particle:Particle) {
        for (seq in particle.animation) for (snap in seq) {
            for (attr in JSObj.fields(snap.interpolators))
                if (getAttribute(system, attr) == null)
                    Error.error('particle ${particle.name} references attribute \'$attr\' not defined in system');
            for (attr in JSObj.fields(snap.attributes)) {
                var sys = getAttribute(system, attr);
                if (sys == null)
                    Error.error('particle ${particle.name} references attribute \'$attr\' not defined in system');
                else {
                    var value = JSObj.field(snap.attributes, attr);
                    Types.checkTypeAssign('particle ${particle.name}', 'attribute \'$attr\'', value, sys.type);
                }
            }
        }
        for (attr in system)
            switch (attr.type) {
            case tTexture(n):
                if (!JSObj.hasField(particle.texuvs, 'texture$n'))
                    JSObj.setField(particle.texuvs, 'texture$n', [[0,0,1,1]]);
            case _:
            }
    }

    static inline function getAttribute(system:System, name:String):Null<Attribute> {
        var ret = null;
        for (attr in system)
            if (attr.name == name) {
                ret = attr;
                break;
            }
        return ret;
    }

}
