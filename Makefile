all:
	rm -rf bin
	mkdir -p bin

	haxe -js bin/particles_gpu.js -main Main -lib turbulenz -dce full
	cp -r ~/Projects/turbulenz/engine/jslib bin/
	cp -r assets bin/

	maketzjs --mode=canvas -t bin -o bin/particles_gpu.canvas.release.js particles_gpu.js # -c ~/Downloads/compiler.jar
	makehtml --mode=canvas       -t bin -C bin/particles_gpu.canvas.release.js particles_gpu.canvas.release.js -o bin/particles_gpu.canvas.release.html
	makehtml --mode=canvas-debug -t bin -C bin/particles_gpu.js                particles_gpu.js                -o bin/particles_gpu.canvas.debug.html

	maketzjs --mode=plugin -t bin -o bin/particles_gpu.release.js        particles_gpu.js # -c ~/Downloads/compiler.jar
	makehtml --mode=plugin       -t bin -C bin/particles_gpu.release.js        particles_gpu.release.js        -o bin/particles_gpu.release.html
	makehtml --mode=plugin-debug -t bin -C bin/particles_gpu.js                particles_gpu.js                -o bin/particles_gpu.debug.html

files=ParticleSystem.hx Encode.hx ParticleBuilder.hx ParticleView.hx ParticleRenderable.hx Packer.hx TimeoutQueue.hx SharedSystemContext.hx
jslib:
	haxe -js particlesystem.js ${files} -lib turbulenz -dce full -D no_requires -D jslib
#	java -jar ~/Downloads/compiler.jar particlesystem.js > particlesystem.minified.js
#	sed -i '1i ;' particlesystem.minified.js
#	# compile with source mapping for non-minified version (Though not presently working on chrome 29 it seems).
#	haxe -js particlesystem.js ${files} -lib turbulenz -dce full -D no_requires -D jslib -debug
#	sed -i '1i ; /*jshint eqnull: true, shadow: true, -W041: false */' particlesystem.js

buildtool:
	haxe -js particle-builder.js -main ParticleBuilder -dce full -D build_tool
	java -jar ~/Downloads/compiler.jar particle-builder.js > particle-builder.minified.js

	haxe -js tp2json.js -main TP2JSON -lib nodejs -dce full

.PHONY: assets
assets:
	./../turbulenz/engine/tools/bin/linux64/cgfx2json -i assets/shaders/shadowmapping.cgfx -o assets/shaders/shadowmapping.cgfx.json
	./../turbulenz/engine/tools/bin/linux64/cgfx2json -i assets/shaders/defaultrendering.cgfx -o assets/shaders/defaultrendering.cgfx.json
	./../turbulenz/engine/tools/bin/linux64/cgfx2json -i assets/shaders/forwardrendering.cgfx -o assets/shaders/forwardrendering.cgfx.json
	./../turbulenz/engine/tools/bin/linux64/cgfx2json -i assets/shaders/debug.cgfx -o assets/shaders/debug.cgfx.json
	./../turbulenz/engine/tools/bin/linux64/cgfx2json -i assets/shaders/zonly.cgfx -o assets/shaders/zonly.cgfx.json
	./../turbulenz/engine/tools/bin/linux64/cgfx2json -i assets/shaders/deferredlights.cgfx -o assets/shaders/deferredlights.cgfx.json
	./../turbulenz/engine/tools/bin/linux64/cgfx2json -i assets/shaders/deferredopaque.cgfx -o assets/shaders/deferredopaque.cgfx.json
	./../turbulenz/engine/tools/bin/linux64/cgfx2json -i assets/shaders/deferredtransparent.cgfx -o assets/shaders/deferredtransparent.cgfx.json
	./../turbulenz/engine/tools/bin/linux64/cgfx2json -i assets/shaders/posteffects.cgfx -o assets/shaders/posteffects.cgfx.json
