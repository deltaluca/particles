package;

@:keep
@:expose("Encode")
class Encode {

    // f in [0,1) map to Int8 value for storage in texture.
    public static function encodeByteUnsignedFloat(f:Float)
        return if (f <= 0.0) 0x00
          else if (f >= 1.0) 0xff
          else Std.int(f * 256.0);

    // f in [-1,1) map to Int8 value for storage in texture.
    public inline static function encodeByteSignedFloat(f:Float)
        return encodeByteUnsignedFloat((f + 1.0) * 0.5);

    // f in [0,1) map to Int16 value for storage in texture.
    public static function encodeHalfUnsignedFloat(f:Float)
        return if (f <= 0.0) 0x0000
          else if (f >= 1.0) 0xffff
          else {
              var r = (f % 1.0) * 256.0;
              var g = ((f * 256.0) % 1.0) * 256.0;
              r -= g / 256.0;
              (Std.int(r) << 8) | Std.int(g);
          };

    public static function decodeHalfUnsignedFloat(i:Int):Float
        return (i & 0xff) / 65536.0 + (i >>> 8) / 256.0;

    // f in [0,1), map to Int32
    public static function encodeUnsignedFloat(f:Float)
        return if (f <= 0.0) 0x00000000;
          else if (f >= 1.0) 0xffffffff;
          else {
              var r = (f % 1.0) * 256.0;
              var g = ((f * 256.0) % 1.0) * 256.0;
              var b = ((f * 65536.0) % 1.0) * 256.0;
              var a = ((f * 16777216.0) % 1.0) * 256.0;
              r -= g / 256.0;
              g -= b / 256.0;
              b -= a / 256.0;
              (Std.int(r) << 24) | (Std.int(g) << 16) | (Std.int(b) << 8) | Std.int(a);
        }

    // f in [0,1) map to Int16
    public static inline function encodeHalfSignedFloat(f:Float)
        return encodeHalfUnsignedFloat((f + 1.0) * 0.5);

    // f in [-1,1), map to Int32 value for storage in texture.
    public static inline function encodeSignedFloat(f:Float)
        return encodeUnsignedFloat((f + 1.0) * 0.5);

    @:extern static inline function saturate(x:Float) return (x < 0.0 ? 0.0 : x > 1.0 ? 1.0 : x);

    // v in [0,1)*4
    public static function encodeUnsignedFloat4(v:Array<Float>) {
        var v0 = v[0];
        var v1 = v[1];
        var v2 = v[2];
        var v3 = v[3];
        return Std.int(saturate(v0) * 0xff) | (Std.int(saturate(v1) * 0xff) << 8) | (Std.int(saturate(v2) * 0xff) << 16) | (Std.int(saturate(v3) * 0xff) << 24);
    }

    // v in [0,1)*2
    public static inline function encodeUnsignedFloat2(v:Array<Float>) {
        var v0 = v[0];
        var v1 = v[1];
        return encodeUnsignedFloat2xy(v0, v1);
    }
    public static inline function encodeUnsignedFloat2xy(x:Float, y:Float)
        return (encodeHalfUnsignedFloat(x) << 16) | encodeHalfUnsignedFloat(y);
    public static inline function decodeUnsignedFloat2(x:Int, ?v:Array<Float>):Array<Float> {
        if (v == null) v = [0.0, 0.0];
        v[0] = decodeHalfUnsignedFloat(x >>> 16);
        v[1] = decodeHalfUnsignedFloat(x & 0xffff);
        return v;
    }

    // v in [-1,1)*2
    public static inline function encodeSignedFloat2(v:Array<Float>)
        return (encodeHalfSignedFloat(v[0]) << 16) | encodeHalfSignedFloat(v[1]);
}
