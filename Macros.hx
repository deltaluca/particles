package;

import haxe.macro.Expr;

class Macros {
    macro public static function parseJSON(path:String) {
        Sys.stdout().writeString("\x1b[31;1m[Macros.hx] \x1b[32;1mParsing JSON\x1b[2m "+path+"\x1b[0m\n");
        var json = sys.io.File.getContent(path+".json");
        return macro $v{std.haxe.Json.parse(json)};
    }
    macro public static function compileShader(path:String) {
        Sys.stdout().writeString("\x1b[31;1m[Macros.hx] \x1b[32;1mCompiling Shader\x1b[2m "+path+"\x1b[0m\n");

        var p = new sys.io.Process("cgfx2json", ["-i", path, "-o", path+".json"]);
        var out = p.stdout.readAll();
        if (out.length != 0) {
            Sys.stdout().writeString("\x1b[31;3m");
            Sys.stdout().write(out);
            Sys.stdout().writeString("\x1b[0m\n");
        }
        if (p.exitCode() != 0) {
            Sys.stdout().writeString("\x1b[31;3m");
            Sys.stdout().write(p.stderr.readAll());
            Sys.stdout().writeString("\x1b[0m\n");
            Sys.exit(1);
        }

        var json = sys.io.File.getContent(path+".json");

        var p = new sys.io.Process("rm", [path+".json"]);
        p.exitCode();

        return macro $v{std.haxe.Json.parse(json)};
    }

    macro public static function or<T>(y:ExprOf<T>, x:ExprOf<T>):ExprOf<T>
        return macro if ($x == null) $y else $x;
}
