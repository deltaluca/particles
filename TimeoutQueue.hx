package;

//
// In a general sense, this is an efficient way of implementing
// a large number of setTimeout operations tied to some update tick
// and hence the name TimeoutQueue, implemented using a binary heap.
//
// In terms of usage, the queue behaves like a typical container
// whose current iterable contents are the elements that have died
// and where iteration destroys the elements as it goes.
//
// Implementation based on the ParticleQueue type.
#if jslib
@:keep
@:expose("TimeoutQueue")
#end
class TimeoutQueue<T> {
    var heap:Array<{ time: Float, data: T }>;
    var heapSize:Int;

    // time since queue created.
    var time:Float;

    inline function swap(i1, i2) {
        var tmp = heap[i1];
        heap[i1] = heap[i2];
        heap[i2] = tmp;
    }

    public function new(initCapacity:Int=1) {
        heap = [];
        for (i in 0...initCapacity) {
            heap.push({
                time: 0.0,
                data: null
            });
        }
        heapSize = 0;
        time = 0.0;
    }
    public function clear() {
        for (h in heap) {
            h.time = 0.0;
            h.data = null;
        }
        heapSize = 0;
        time = 0.0;
    }

    // Remove element from binary heap at some location 'i'
    //   and re-insert it again with new time value.
    inline function replace(i:Int, time:Float, data:T) {
        // Swap element with last in heap.
        //   and filter down to correct position.
        var h2 = heapSize - 1;
        if (i != h2) {
            swap(i, h2);
            while (true) {
                var left  = (i << 1) + 1;
                var right = (i << 1) + 2;
                var small = i;
                if (left  < h2 && heap[left].time  < heap[small].time) small = left;
                if (right < h2 && heap[right].time < heap[small].time) small = right;
                if (i == small) break;
                swap(i, small);
                i = small;
            }
        }

        // set new time for last element in heap.
        // and filter back up to correct position.
        i = h2;
        heap[i].time = time;
        heap[i].data = data;
        if (i != 0) {
            var parent = (i - 1) >>> 1;
            while (parent != i && heap[i].time < heap[parent].time) {
                swap(i, parent);
                i = parent;
                if (parent == 0) break;
                parent = (parent - 1) >>> 1;
            }
        }
    }

    inline function find(data:T):Int {
        var i = 0;
        while (i < heapSize)
            if (heap[i].data == data) break;
            else i++;
        return i;
    }

    public function remove(data:T) {
        // Re-insert data with time = '-time'
        //   so that it <= every item and will be sent to the
        //   root of the heap.
        replace(find(data), -time, null);
        heapSize--;
    }

    public function insert(data:T, timeTillDeath:Float) {
        var deathTime = timeTillDeath + time;
        // Push to back of heap, and re-insert.
        if (heapSize == heap.length) {
            // Grow capacity
            heap.push({
                time: 0.0,
                data: null
            });
        }
        heapSize++;
        replace(heapSize - 1, deathTime, data);
    }

    public inline function update(timeUpdate:Float) {
        time += timeUpdate;
        return this;
    }

    public inline function hasNext()
        return heapSize != 0 && heap[0].time <= time;
    public inline function next():T {
        var ret = heap[0].data;
        replace(0, Math.POSITIVE_INFINITY, null);
        heapSize--;
        return ret;
    }

#if !jslib
    // Haxe iterator
    public inline function iterator()
        return {
            hasNext: hasNext,
            next: next
        };
    }
#end

    // more efficient for use in JS due to not being able to inline things anymore.
    public function iter<S>(f:T->S) {
        while (hasNext()) f(next());
    }
}
