package;

import js.html.Float32Array;

typedef ParticleID = Int;
typedef PrintedNode = {
    node:  String,
    left:  Array<String>,
    right: Array<String>
};

//
// Compressed, binary heap
// root of heap is the particle index corresponding to the earliest
// particle death, so that when we force creation and overwrite an
// existing particle, we replace the one which would next have died
// and minimise impact on the visual appearance of a system.
//
class ParticleQueue {
    // (time, index) pair list
    var heap:Float32Array;
    var heapSize:Int;

    // Time since queue created
    var time:Float;

    inline function swap(i1, i2) {
        var tmp = heap[i1];
        heap[i1] = heap[i2];
        heap[i2] = tmp;

        var tmp  = heap[i1 + 1];
        heap[i1 + 1] = heap[i2 + 1];
        heap[i2 + 1] = tmp;
    }

    public function new(maxParticles:Int) {
        heap = new Float32Array(heapSize = maxParticles << 1);
        for (i in 0...maxParticles) heap[(i << 1) + 1] = i;
        time = 0.0;
    }
    public function clear() {
        for (i in 0...(heapSize>>>1)) heap[(i << 1)] = 0.0;
        time = 0.0;
    }

    // Remove element from binary heap at some location 'i'
    //   and re-insert it again with new time value.
    inline function replace(i:Int, time:Float) {
        // Swap element with last in heap.
        //   and filter down to correct position.
        var h2 = heapSize - 2;
        if (i != h2) {
            swap(i, h2);
            while (true) {
                var left  = (i << 1) + 2;
                var right = (i << 1) + 4;
                var small = i;
                if (left  < h2 && heap[left]  < heap[small]) small = left;
                if (right < h2 && heap[right] < heap[small]) small = right;
                if (i == small) break;
                swap(i, small);
                i = small;
            }
        }

        // set new time for last element in heap.
        // and filter back up to correct position.
        i = h2;
        heap[i+0] = time;
        if (i != 0) {
            var parent = ((i - 2) >>> 2) << 1;
            while (parent != i && heap[i] < heap[parent]) {
                swap(i, parent);
                i = parent;
                if (parent == 0) break;
                parent = ((parent - 2) >>> 2) << 1;
            }
        }
        return untyped heap[i + 1];
    }

    inline function find(particleID:Int):Int {
        var i = 0;
        while (i < heapSize)
            if (heap[i + 1] == particleID) break;
            else i += 2;
        return i;
    }

    public function removeParticle(particleID:Int) {
        // Re-insert particle with time = '-time'
        //   so that it <= every particle and will be sent to the
        //   root of the heap.
        replace(find(particleID), -time);
    }

    public function updateParticle(particleID:Int, lifeDelta:Float) {
        var i = find(particleID);
        var deathTime = heap[i] + lifeDelta;
        if (deathTime > lastDeath)
            lastDeath = deathTime;
        replace(i, deathTime);
    }

    public var wasForced:Bool;
    var lastDeath:Float = -1.0;
    public function create(timeTillDeath:Float, forceCreation:Bool=false):Null<Int> {
        if (forceCreation || (heap[0] <= time)) {
            wasForced = (heap[0] > time);
            // Remove root, and re-insert with new time value.
            var id:Int = untyped heap[1];
            var deathTime = timeTillDeath + time;
            if (deathTime > lastDeath)
                lastDeath = deathTime;
            replace(0, deathTime);
            return id;
        }
        else return null;
    }

    // Returns if - after system update - there is any potentially live particles remaining.
    public inline function update(timeUpdate:Float) {
        time += timeUpdate;
        return time <= lastDeath;
    }

    // Used only for debugging, checking heap property is defined.
    public function verify(i=0) {
        if (i < heapSize) {
            var left  = (i << 1) + 2;
            var right = (i << 1) + 4;
            if (left < heapSize) {
                if (heap[i] > heap[left]) throw "left subtree violated";
                verify(left);
            }
            if (right < heapSize) {
                if (heap[i] > heap[right]) throw "right subtree violated";
                verify(right);
            }
        }
    }

    // Used only for debugging, building graphic representation of tree.
    public function toString() {
        function print(i:Int) {
            var id = '${heap[i+1]}';
            while (id.length < 3) id = ' $id';
            var time = '${heap[i]-time}'.substr(0,6);
            while (time.length < 6) {
                time = '$time ';
                if (time.length < 6) time = ' $time';
            }
            return '$id ($time)';
        }

        var up = ['|','^'];
        var down = ['|','v'];

        var printTree = null;
        printTree = function(i:Int):PrintedNode {
            var stack = [print(i)];
            var left  = (i << 1) + 2;
            var right = (i << 1) + 4;
            var left :PrintedNode = if (left  < heapSize) printTree(left)  else null;
            var right:PrintedNode = if (right < heapSize) printTree(right) else null;
            if (left != null) {
                left.left = [for (x in left.left) '                  ' + x];
                left.node = '      |---------- ' + left.node;
                left.right = [for (i in 0...left.right.length) '      ${up[i%2]}           ' + left.right[i]];
            }
            if (right != null) {
                right.left = [for (i in 0...right.left.length) '      ${down[i%2]}           ' + right.left[i]];
                right.node = '      |---------- ' + right.node;
                right.right = [for (x in right.right) '                  ' + x];
            }
            return {
                node: stack[0],
                left:  if (left  == null) [] else left.left.concat([left.node]).concat(left.right),
                right: if (right == null) [] else right.left.concat([right.node]).concat(right.right)
            }
        }
        var node = printTree(0);
        return node.left.concat([node.node]).concat(node.right).join("\n");
    }
}
