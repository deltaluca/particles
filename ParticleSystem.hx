package;

import turbulenz.graphics.DrawParameters;
import turbulenz.graphics.GraphicsDevice;
import turbulenz.graphics.RenderTarget;
import turbulenz.graphics.Semantics;
import turbulenz.graphics.Shader;
import turbulenz.graphics.Technique;
import turbulenz.graphics.TechniqueParameters;
import turbulenz.graphics.Texture;
import turbulenz.graphics.VertexBuffer;
import turbulenz.MathDevice;
import turbulenz.TurbulenzEngine;
import turbulenz.util.TZArray;

import js.html.Float32Array;
import js.html.Uint32Array;
import js.html.Uint16Array;
import js.html.Uint8Array;

import ParticleQueue.ParticleID;
import SharedSystemContext;

typedef ParticleUpdater = {
    technique : Technique,
    parameters: Dynamic,
    update    : Float32Array -> Uint32Array -> Uint16Array -> Int -> TechniqueParameters -> Int,
    predict   : Vector3 -> Vector3 -> UInt32 -> Float -> Void
};
typedef ParticleRenderer = {
    technique : Technique,
    parameters: Dynamic
};

#if jslib
@:keep
@:expose("ParticleSystem")
#end
@:allow(ParticleView)
@:allow(SharedSystemContext)
class ParticleSystem {
    // CPU particle storage storage indices
    static inline var PARTICLE_SPAN = 9; // size of particle in cpu memory
    static inline var PARTICLE_POS  = 0; // offset to access non-encoded clamped position (+1,+2)
    static inline var PARTICLE_VEL  = 3; // offset to access non-encoded clamped velocity (+1,+2)
    static inline var PARTICLE_LIFE = 6; // offset to access encoded (life | maxLife)
    static inline var PARTICLE_ANIM = 7; // offset to access encoded (c0, c1) animation range coefficients.
    static inline var PARTICLE_DATA = 8; // offset to access userData int32

    // GPU particle storage
    static inline var PARTICLE_DIM = 3; // each particle is a 3x3 block in texture.

    // A default, all white render texture for particles.
    static var defaultRenderTexture:Texture = null;
    public static function getDefaultRenderTexture(graphicsDevice:GraphicsDevice) {
        if (defaultRenderTexture != null) return defaultRenderTexture;
        return defaultRenderTexture = graphicsDevice.createTexture({
            name      : "ParticleSystem defaultRenderTexture",
            width     : 1,
            height    : 1,
            depth     : 1,
            format    : graphicsDevice.PIXELFORMAT_L8,
            mipmaps   : true,
            cubemap   : false,
            renderable: false,
            _dynamic  : false,
            data      : [0xff]
        });
    }

    // A default, all zeroes noise texture for particles.
    static var defaultNoiseTexture:Texture = null;
    public static function getDefaultNoiseTexture(graphicsDevice:GraphicsDevice) {
        if (defaultNoiseTexture != null) return defaultNoiseTexture;
        return defaultNoiseTexture = graphicsDevice.createTexture({
            name      : "ParticleSystem defaultNoiseTexture",
            width     : 1,
            height    : 1,
            depth     : 1,
            format    : graphicsDevice.PIXELFORMAT_R8G8B8A8,
            mipmaps   : true,
            cubemap   : false,
            renderable: false,
            _dynamic  : false,
            data      : [for (i in 0...4) Encode.encodeByteSignedFloat(0.0)]
        });
    }

    // System default update technique
    static var defaultUpdateTechnique:Technique = null;
    public static function createDefaultUpdater(
        graphicsDevice: GraphicsDevice,
        params: {
            ?acceleration         : Vector3,
            ?drag                 : Float,
            ?noiseTexture         : Texture,
            ?randomizeAcceleration: Vector3
        }
    ):ParticleUpdater {
        if (defaultUpdateTechnique == null) {
            // Default shader is compiled into js source at compile time.
            defaultUpdateTechnique =
                graphicsDevice.createShader(Macros.compileShader('particles-default-update.cgfx'))
                              .getTechnique("update_state");
        }
        return {
            technique: defaultUpdateTechnique,
            parameters: {
                acceleration: Macros.or(([0,0,0] : Vector3), params.acceleration),
                drag        : Macros.or(0.0, params.drag),
                noiseTexture: Macros.or(getDefaultNoiseTexture(graphicsDevice), params.noiseTexture),
                randomizeAcceleration: Macros.or(([0,0,0] : Vector3), params.randomizeAcceleration)
            },
            predict: function (
                pos     : Vector3,
                vel     : Vector3,
                userData: UInt32,
                time    : Float
            ) {
                // A rough approximation only!
                // Not possibly to determine analytically
                // especcially since non-constant step sizes would just not possible to analyse.

                // To permit approximation, we assume a particles position/velocity
                // does not reach maximum values at any point
                // We assume that the randomised acceleration is uniformnly random
                // and can be disregarded.
                // We assume that the system is updated with a constant timestep of 'h'
                // In this case, we can deduce analytically:
                // v(t) = d^(t/h)v + ah(sum(t/h))
                //     where d = 1 - min(1, drag.h)
                //     and sum(n) = if (d <> 1) d(d^n-1)/(d-1) else nd

                // if d = 1, we have uniform acceleration and
                // p(t) ~= p(0) + v(0)*t + at^2/2
                //
                // if d <> 1, then by wolfram alpha
                // p(t) ~= p(0) + h(d^(t/h) -1)/(log d)*v + (h(d^(t/h) -1) - log d)/((d-1)log d)adh
                //
                // we note that using integratino to compute position estimate is not strictly
                // correct due to euler integration used in actual simulation.

                var h = 1/60; // a reasonable guess at time step I would say in ideal circumstances.
                var acceleration = params.acceleration;
                var drag = (1 - Math.min(1, params.drag * h));
                if (drag == 1) {
                    pos[0] += time * (vel[0] + time * 0.5 * acceleration[0]);
                    pos[1] += time * (vel[1] + time * 0.5 * acceleration[1]);
                    pos[2] += time * (vel[2] + time * 0.5 * acceleration[2]);
                    vel[0] += time * acceleration[0];
                    vel[1] += time * acceleration[1];
                    vel[2] += time * acceleration[2];
                }
                else {
                    var pow = Math.pow(drag, time / h);
                    var log = Math.log(drag);
                    var coef = h * (pow - 1) / log;
                    var coef2 = (h * (pow - 1) - time * log) / ((drag - 1) * log) * drag * h;
                    var coef3 = h * drag * (pow - 1) / (drag - 1);
                    pos[0] += coef * vel[0] + coef2 * acceleration[0];
                    pos[1] += coef * vel[1] + coef2 * acceleration[1];
                    pos[2] += coef * vel[2] + coef2 * acceleration[2];
                    vel[0] = pow * vel[0] + coef3 * acceleration[0];
                    vel[1] = pow * vel[1] + coef3 * acceleration[1];
                    vel[2] = pow * vel[2] + coef3 * acceleration[2];
                }
            },
            update: function (
                data      : Float32Array,
                dataI     : Uint32Array,
                tracked   : Uint16Array,
                numTracked: Int,
                params    : TechniqueParameters
            ) {
                var timeStep    :Float   = params.timeStep;
                var lifeStep    :Float   = params.lifeStep;
                var acceleration:Vector3 = params.acceleration;
                var drag        :Float   = params.drag;
                var center      :Vector3 = params.center;
                var halfExtents :Vector3 = params.halfExtents;
                var shift       :Vector3 = params.shift;

                drag = (1 - Math.min(1, timeStep * drag));

                var ax = acceleration.x / halfExtents.x;
                var ay = acceleration.y / halfExtents.y;
                var az = acceleration.z / halfExtents.z;

                var sx = shift.x;
                var sy = shift.y;
                var sz = shift.z;

                var store = 0;
                for (i in 0...numTracked) {
                    var index = tracked[i] * PARTICLE_SPAN;

                    // Compute next life, kill particle if expired.
                    var life = Encode.decodeHalfUnsignedFloat(dataI[index + PARTICLE_LIFE] >>> 16) - lifeStep;
                    if (life <= 0.0) continue;

                    tracked[store] = tracked[i];
                    store++;

                    var vx = data[index + PARTICLE_VEL+0];
                    var vy = data[index + PARTICLE_VEL+1];
                    var vz = data[index + PARTICLE_VEL+2];

                    // Update position
                    var x = data[index + PARTICLE_POS+0] + (vx * timeStep);
                    var y = data[index + PARTICLE_POS+1] + (vy * timeStep);
                    var z = data[index + PARTICLE_POS+2] + (vz * timeStep);
                    data[index + PARTICLE_POS+0] = clamp(-1,1, x + sx);
                    data[index + PARTICLE_POS+1] = clamp(-1,1, y + sy);
                    data[index + PARTICLE_POS+2] = clamp(-1,1, z + sz);

                    // Update velocity
                    var x = drag * (vx + (ax * timeStep));
                    var y = drag * (vy + (ay * timeStep));
                    var z = drag * (vz + (az * timeStep));
                    data[index + PARTICLE_VEL+0] = clamp(-1,1, x);
                    data[index + PARTICLE_VEL+1] = clamp(-1,1, y);
                    data[index + PARTICLE_VEL+2] = clamp(-1,1, z);

                    // Update life
                    dataI[index + PARTICLE_LIFE] = (Encode.encodeHalfUnsignedFloat(life) << 16) |
                                                   (0xffff & dataI[index + PARTICLE_LIFE]);
                }
                return store;
            }
        };
    }
    // Careful to ensure the 'seed' parameter uses the same data store
    // as the default renderer for compatibility without wasting space.
    public static function buildDefaultUpdaterData(params:{
        ?randomizeAcceleration:Bool, // false
        ?seed: UInt8 // Math.random()*0xff
    }):Int {
        var ret = 0;
        if (params.randomizeAcceleration) ret |= 1 << 25;
        if (params.seed != null) ret |= params.seed << 16;
        return ret;
    }

    static var defaultRenderShader:Shader = null;
    public static function createDefaultRenderer(
        graphicsDevice: GraphicsDevice,
        attributes: Dynamic<ParticleBuilder.AttributeRange>,
        params: {
            ?blendMode           : String, // 'alpha' 'opaque' 'additive'
            ?texture             : Texture,
            ?noiseTexture        : Texture,
            ?randomizeOrientation: Vector2,
            ?randomizeScale      : Vector2,
            ?randomizeRotation   : Float,
            ?randomizeAlpha      : Float,
            ?animateOrientation  : Bool,
            ?animateScale        : Bool,
            ?animateRotation     : Bool,
            ?animateAlpha        : Bool
        }
    ): ParticleRenderer {
        if (defaultRenderShader == null) {
            // Default shader is compiled into js source at compile time.
            defaultRenderShader =
                graphicsDevice.createShader(Macros.compileShader('particles-default-render.cgfx'));
        }

        if (!Reflect.hasField(attributes, "scale"))
            throw "Default Renderer requires normalized scale attribute mapping coefficients";
        if (!Reflect.hasField(attributes, "rotation"))
            throw "Default Renderer requires normalized rotation attribute mapping coefficients";

        var scale:{min:Array<Float>, delta:Array<Float>} = cast attributes.scale;
        var rotation:{min:Float, delta:Float} = cast attributes.rotation;
        var noiseTexture = Macros.or(getDefaultNoiseTexture(graphicsDevice), params.noiseTexture);

        return {
            technique: defaultRenderShader.getTechnique(Macros.or("alpha", params.blendMode)),
            parameters: {
                animationScale      : ([scale.min[0], scale.min[1], scale.delta[0], scale.delta[1]] : Vector4),
                animationRotation   : ([rotation.min, rotation.delta] : Vector2),
                texture             : Macros.or(getDefaultRenderTexture(graphicsDevice), params.texture),
                fnoiseTexture       : noiseTexture,
                vnoiseTexture       : noiseTexture,
                randomizeOrientation: Macros.or(([0,0] : Vector2), params.randomizeOrientation),
                randomizeScale      : Macros.or(([0,0] : Vector2), params.randomizeScale),
                randomizeRotation   : Macros.or(0.0, params.randomizeRotation),
                randomizeAlpha      : Macros.or(0.0, params.randomizeAlpha),
                animateOrientation  : Macros.or(false, params.animateOrientation),
                animateScale        : Macros.or(false, params.animateScale),
                animateRotation     : Macros.or(false, params.animateRotation),
                animateAlpha        : Macros.or(false, params.animateAlpha)
            }
        };
    }
    public static function buildDefaultRendererData(params:{
        ?facing              : String,// 'billboard' 'velocity' 'custom', default = billboard
        ?randomizeOrientation: Bool,  // false
        ?randomizeScale      : Bool,  // false
        ?randomizeRotation   : Bool,  // false
        ?randomizeAlpha      : Bool,  // false
        ?seed                : UInt8, // Math.random()*0xff
        ?phi                 : Float, // 0
        ?theta               : Float  // 0
    }):Int {
        var ret = 0;
        if      (params.facing == 'velocity') ret |= 1<<30;
        else if (params.facing == 'custom')   ret |= 2<<30;

        if (params.randomizeRotation)    ret |= 1 << 29;
        if (params.randomizeScale)       ret |= 1 << 28;
        if (params.randomizeOrientation) ret |= 1 << 27;
        if (params.randomizeAlpha)       ret |= 1 << 26;

        if (params.seed != null) ret |= params.seed << 16;

        // allow phi/theta to be given as arbitrary angles
        // mapped to appropriate bounded phi/theta before
        // eventual mappings to [0,1) ranges.
        var phiDelta = 0.0;
        if (params.theta != null) {
            var theta = (params.theta % (Math.PI*2));
            if (theta < 0) theta += Math.PI*2;
            if (theta > Math.PI) {
                theta = Math.PI*2 - theta;
                phiDelta = Math.PI;
            }
            ret |= Encode.encodeByteUnsignedFloat(theta / Math.PI) << 8;
        }
        if (params.phi != null || phiDelta != 0.0) {
            var phi = (Macros.or(0.0, params.phi) + phiDelta) % (Math.PI*2);
            if (phi < 0) phi += Math.PI*2;
            ret |= Encode.encodeByteUnsignedFloat(phi / (Math.PI*2));
        }

        return ret;
    }

    // Ensure inlined no matter what to preserve lazy semantics
    @:extern static inline function clamp(x0:Float, x1:Float, x:Float)
        return if (x < x0) x0 else if (x > x1) x1 else x;

    // Internal techniques used only by the particle system
    static var prepareSortTechnique:Technique;
    static var mergeSortTechnique  :Technique;
    static function initSortTechniques(graphicsDevice:GraphicsDevice) {
        if (prepareSortTechnique != null) return;

        var sortShader = graphicsDevice.createShader(Macros.compileShader('particles-sort.cgfx'));
        prepareSortTechnique = sortShader.getTechnique('prepare_sort');
        mergeSortTechnique   = sortShader.getTechnique('sort_pass');
    }

    static var debugParticlesTechnique :Technique;
    static var debugMappingTechnique   :Technique;
    static var debugMappingParameters  :TechniqueParameters;
    static var debugParticlesParameters:TechniqueParameters;
    static function initDebugTechniques(graphicsDevice:GraphicsDevice) {
        if (debugParticlesTechnique != null) return;

        var debugShader = graphicsDevice.createShader(Macros.compileShader('particles-debug.cgfx'));
        debugParticlesTechnique = debugShader.getTechnique('debug_particles');
        debugMappingTechnique   = debugShader.getTechnique('debug_mapping');

        debugMappingParameters = graphicsDevice.createTechniqueParameters({
            texture       : (null : Texture),
            renderQuad    : (null : Vector4)
        });
        debugParticlesParameters = graphicsDevice.createTechniqueParameters({
            textureSize   : (null : Vector2),
            invTextureSize: (null : Vector2),
            regionSize    : (null : Vector2),
            invRegionSize : (null : Vector2),
            regionPos     : (null : Vector2)
        });
    }

    // --------------------------------------------------------------------------------------------------------

    // Public properties, mostly read-only after creation.
    @:isVar public var maxParticles(default,null):Int;
    @:isVar public var center      (default,null):Vector3;
    @:isVar public var halfExtents (default,null):Vector3;
    @:isVar public var maxLifeTime (default,null):Float;
    @:isVar public var particleSize(default,null):{x:Int, y:Int};
    @:isVar public var updater     (default,null):ParticleUpdater;
    @:isVar public var renderer    (default,null):ParticleRenderer;
    @:isVar public var zSorted     (default,null):Bool;
    public var synchronize:ParticleSystem->Int->Float->Void;
    public var maxSortSteps:Int;

    @:isVar public var views(default,null):Array<ParticleView>;

    // --------------------------------------------------------------------------------------------------------

    var graphicsDevice:GraphicsDevice;
    var mathDevice    :MathDevice;
    var invHExtents   :Vector3;

    // Particle state texture/render targets.
    // Current state/target given by current index.
    var sharedContext:Bool;
    var context:SystemContext;
    var current:Int = 0;
    public function updateSystemContext(context:SystemContext)
    {
        this.context = context;
        if (context != null) {
            var tex = context.textures[0];
            var uv = context.uvRect;
            var ts  :Vector2 = [tex.width, tex.height];
            var its :Vector2 = [1 / ts.x, 1 / ts.y];
            var rp  :Vector2 = [uv.x, uv.y];

            // TODO only need to update region size/invSize
            //      when parametrs is first constructed as this doesn't change
            //      when shared context is resized.
            var rs  :Vector2 = [uv.z - uv.x, uv.w - uv.y];
            var irs :Vector2 = [1 / rs.x, 1 / rs.y];

            renderParameters.textureSize    = ts;
            renderParameters.invTextureSize = its;
            renderParameters.regionSize     = rs;
            renderParameters.invRegionSize  = irs;
            renderParameters.regionPos      = rp;
            for (v in views) {
                v.renderParameters.textureSize    = ts;
                v.renderParameters.invTextureSize = its;
                v.renderParameters.regionSize     = rs;
                v.renderParameters.invRegionSize  = irs;
                v.renderParameters.regionPos      = rp;
                v.renderParameters.vparticleState = context.textures[current];
                v.renderParameters.fparticleState = context.textures[current];
            }

            updateParameters.textureSize    = ts;
            updateParameters.invTextureSize = its;
            updateParameters.regionSize     = rs;
            updateParameters.invRegionSize  = irs;
            updateParameters.regionPos      = rp;

            if (zSorted) {
                prepareSortParameters.textureSize    = ts;
                prepareSortParameters.invTextureSize = its;
                prepareSortParameters.regionSize     = rs;
                prepareSortParameters.invRegionSize  = irs;
                prepareSortParameters.regionPos      = rp;

                mergeSortParameters.regionSize     = rs;
                mergeSortParameters.invRegionSize  = irs;
            }
        }
    }

    // Attributes for rendering particles.
    var numParticleVertices:Int;
    var sharedGeometry     :Bool;
    var particleVertices   :VertexBuffer;
    var particleSemantics  :Semantics;
    var animationTexture   :Texture;

    // Technique parameters
    var mergeSortParameters     :TechniqueParameters;
    var prepareSortParameters   :TechniqueParameters;
    var renderParameters        :TechniqueParameters;
    var updateParameters        :TechniqueParameters;
    var maxMergeStage           :Int;

    // CPU tracked particles
    public var trackingEnabled(default,null):Bool;
    var numTracked:Int;
    var tracked   :Uint16Array; // mapping table index storing tracked particles 'only'.
    var cpu       :Float32Array; // storage is linear (PARTICLE_STRIDE) on cpu side instead of boxed like gpu.
    var cpuI      :Uint32Array; // buffer view onto cpu

    // CPU created particles
    var numCreated      :Int;
    var createdIndex    :Uint16Array; // index of created particles since last update
    var createData      :Uint8Array;    // buffer used to update creation texture on GPU.
    var createData32    :Uint32Array; // buffer view onto createData
    var createTexture   :Texture;
    var createForceFlush:Bool;

    // Queue storing available particle slots in time order.
    var queue:ParticleQueue;

    // Renderable sync updates.
    var lastVisible:Int;
    var lastTime   :Float;
    var timer      :Void->Float;

    // --------------------------------------------------------------------------------------------------------

    static var textureVertices :VertexBuffer;
    static var textureSemantics:Semantics;

    public static function createSharedParticleGeometry(
        graphicsDevice   : GraphicsDevice,
        maxParticles     : Int,
        ?particleGeometry: {
            constructor: Int->Dynamic,
            template   : Array<Dynamic>,
            attributes : Array<VertexFormat>,
            semantics  : Semantics,
            stride     : Int
        }
    ) {
        var constructor:Int->Dynamic;
        var template   :Array<Dynamic>;
        var attributes :Array<VertexFormat>;
        var stride     :Int;
        var semantics  :Semantics;
        if (particleGeometry == null) {
            constructor = function (size) return new Uint16Array(size);
            template    = [0,null, 1,null, 2,null,
                           0,null, 2,null, 3,null];
            attributes  = [graphicsDevice.VERTEXFORMAT_USHORT2];
            stride      = 2;
            semantics   = graphicsDevice.createSemantics([graphicsDevice.SEMANTIC_TEXCOORD]);
        }
        else {
            constructor = particleGeometry.constructor;
            template    = particleGeometry.template;
            attributes  = particleGeometry.attributes;
            stride      = particleGeometry.stride;
            semantics   = particleGeometry.semantics;
        }

        // Use template to fill out vertex data
        var particleData = constructor(maxParticles * template.length);
        for (i in 0...maxParticles) {
            var index = (i * template.length);
            for (j in 0...template.length)
                particleData[index + j] = Macros.or(i, template[j]);
        }

        // Create rendering data
        var particleStride = Std.int(template.length / stride);
        var particleVertices = graphicsDevice.createVertexBuffer({
            numVertices: maxParticles * particleStride,
            attributes : attributes,
            _dynamic   : false,
            data       : particleData
        });

        return {
            vertexBuffer  : particleVertices,
            semantics     : semantics,
            particleStride: particleStride
        };
    }

    public static function computeMaxParticleDependents(maxParticles:Int, zSorted:Bool) {
        if (zSorted) {
            if (maxParticles == null || maxParticles <= 8)
                return {
                    maxMergeStage: 2,
                    textureSize  : {x: 4, y: 2},
                    maxParticles : 8
                };
            else {
                // Find best textureSize (most square-like) just large enough for
                // maxParticles, and with area as 8 * power of 2 for sorting.
                var n = Math.ceil(Math.log(maxParticles) / Math.log(2));
                if (n < 3)
                    return {
                        maxMergeStage: 2,
                        textureSize  : {x: 4, y: 2},
                        maxParticles : 8,
                    };
                else if (n > 16)
                    return {
                        maxMergeStage: 15,
                        textureSize  : {x: (1 << 8), y: (1 << 8)},
                        maxParticles : (1 << 16)
                    };
                else {
                    var dim = (n >>> 1);
                    return {
                        maxMergeStage: (n - 1),
                        textureSize  : {x: (1 << (n - dim)), y: (1 << dim)},
                        maxParticles : maxParticles,
                    };
                }
            }
        }
        else {
            // No restrictions for sorting, can make optimise use of space.
            if (maxParticles == null) {
                return {
                    maxMergeStage: null,
                    textureSize  : {x: 4, y: 2},
                    maxParticles     : 8
                };
            }
            else {
                // Find square like texture size fitting maxParticles
                // to aid in shared packing of textures.
                if (maxParticles > 66536)
                    maxParticles = 66536;
                var dimx = Math.ceil(Math.sqrt(maxParticles));
                var dimy = Math.ceil(maxParticles / dimx);
                return {
                    maxMergeStage: null,
                    textureSize  : {x: dimx, y: dimy},
                    maxParticles     : dimx * dimy
                };
            }
        }
    }

    public function new(params:{
        graphicsDevice   : GraphicsDevice,
        mathDevice       : MathDevice,
        ?center          : Vector3,
        halfExtents      : Vector3,
        maxLifeTime      : Float,
        animationTexture : Texture,
        updater          : ParticleUpdater,
        renderer         : ParticleRenderer,
        ?maxParticles    : Int,
        ?zSorted         : Bool,
        ?maxSortSteps    : Int,
        ?particleGeometry: {
            constructor: Int->Dynamic,
            template   : Array<Dynamic>,
            attributes : Array<VertexFormat>,
            semantics  : Semantics,
            stride     : Int
        },
        ?sharedParticleGeometry: {
            vertexBuffer  : VertexBuffer,
            semantics     : Semantics,
            particleStride: Int
        },
        ?synchronize     : ParticleSystem->Int->Float->Void,
        ?timer           : Void->Float,
        ?context         : SharedSystemContext,
        ?trackingEnabled : Bool
    }) {
        graphicsDevice   = params.graphicsDevice;
        mathDevice       = params.mathDevice;
        center           = Macros.or(([0,0,0] : Vector3), params.center);
        halfExtents      = params.halfExtents;
        maxLifeTime      = params.maxLifeTime;
        animationTexture = params.animationTexture;
        updater          = params.updater;
        renderer         = params.renderer;
        maxSortSteps     = Macros.or(20, params.maxSortSteps);
        synchronize      = params.synchronize;
        zSorted          = Macros.or(false, params.zSorted);
        this.timer       = Macros.or(function () return TurbulenzEngine.time, timer);

        invHExtents = [1 / halfExtents.x, 1 / halfExtents.y, 1 / halfExtents.z];

        // Compute textureSize/max particle maxParticles and maximum merge stages for sorting.
        // Merge sort requires a power of 2 at least 8 for the maximum maxParticles of particles
        // ---------------------------------------------------------------------------------
        var deps = computeMaxParticleDependents(params.maxParticles, zSorted);
        particleSize = deps.textureSize;
        maxMergeStage = deps.maxMergeStage;
        if (params.maxParticles != null)
            maxParticles = Std.int(Math.min(params.maxParticles, deps.maxParticles));
        else
            maxParticles = deps.maxParticles;

        // Create particle renderering data
        // --------------------------------
        var geom;
        if (params.sharedParticleGeometry != null) {
            geom = params.sharedParticleGeometry;
            sharedGeometry = true;
        }
        else {
            geom = createSharedParticleGeometry(graphicsDevice, maxParticles, params.particleGeometry);
            sharedGeometry = false;
        }
        numParticleVertices = maxParticles * geom.particleStride;
        particleVertices    = geom.vertexBuffer;
        particleSemantics   = geom.semantics;

        // Create View renderParameters object.
        views = [];
        var parameters = Reflect.copy(renderer.parameters);
        parameters.center         = center;
        parameters.halfExtents    = halfExtents;
        parameters.animation      = animationTexture;
        parameters.animationSize  = ([animationTexture.width, animationTexture.height] : Vector2);
        parameters.modelView      = (null : Matrix34);
        parameters.projection     = (null : Matrix44);
        parameters.mappingTable   = (null : Texture);
        parameters.fparticleState = (null : Texture);
        parameters.vparticleState = (null : Texture);
        parameters.zSorted        = zSorted;
        renderParameters = graphicsDevice.createTechniqueParameters(parameters);

        // Create texture rendering data
        // -----------------------------
        if (textureVertices == null) {
            textureVertices = graphicsDevice.createVertexBuffer({
                numVertices: 4,
                attributes : [graphicsDevice.VERTEXFORMAT_FLOAT2],
                _dynamic   : false,
                data       : [0,0, 1,0, 0,1, 1,1]
            });
            textureSemantics = graphicsDevice.createSemantics([graphicsDevice.SEMANTIC_POSITION]);
        }

        // Create particle creation data
        // -----------------------------
        numCreated = 0;
        createdIndex = new Uint16Array(4);
        createData   = new Uint8Array(particleSize.x * particleSize.y * PARTICLE_DIM * PARTICLE_DIM * 4);
        createData32 = new Uint32Array(createData.buffer);
        createTexture = graphicsDevice.createTexture({
            name      : 'ParticleSystem Creation Texture',
            width     : particleSize.x * PARTICLE_DIM,
            height    : particleSize.y * PARTICLE_DIM,
            depth     : 1,
            format    : graphicsDevice.PIXELFORMAT_R8G8B8A8,
            mipmaps   : false,
            cubemap   : false,
            renderable: false,
            _dynamic  : true,
            data      : createData
        });
        queue = new ParticleQueue(maxParticles);

        // Create particle cpu tracking data
        // ---------------------------------
        trackingEnabled = Macros.or(false, params.trackingEnabled);
        if (trackingEnabled) {
            numTracked = 0;
            tracked = new Uint16Array(2);
            cpu  = new Float32Array(particleSize.x * particleSize.y * PARTICLE_DIM * PARTICLE_DIM);
            cpuI = new Uint32Array(cpu.buffer);
        }

        if (zSorted) {
            // Create sorting data
            // -------------------
            mergeSortParameters = graphicsDevice.createTechniqueParameters({
                cpass         : 0.0,
                PmS           : 0.0,
                twoStage      : 0.0,
                twoStage_PmS_1: 0.0,
                mappingTable  : (null : Texture)
            });
            prepareSortParameters = graphicsDevice.createTechniqueParameters({
                modelView     : (null : Matrix43),
                mappingTable  : (null : Texture),
                particleState : (null : Texture),
                zBound        : 0.0
            });
        }

        // Create update data
        // ------------------
        var parameters = Reflect.copy(updater.parameters);
        parameters.timeStep       = 0.0;
        parameters.lifeStep       = 0.0;
        parameters.shift          = ([0,0,0] : Vector3);
        parameters.center         = center;
        parameters.halfExtents    = halfExtents;
        parameters.previousState  = (null : Texture);
        parameters.creationState  = createTexture;
        updateParameters = graphicsDevice.createTechniqueParameters(parameters);

        // Create particle states
        // ----------------------
        sharedContext = (params.context != null);
        updateSystemContext(if (sharedContext) params.context.alloc(this)
                            else SharedSystemContext.unsharedContext(graphicsDevice, particleSize));

        // Renderable sync updates
        lastVisible = null;
        lastTime = this.timer();
    }
    public function destroy() {
        if (!sharedContext)
            SharedSystemContext.destroyUnsharedContext(context);
        if (!sharedGeometry)
            particleVertices.destroy();
        for (v in views)
            v.destroy();
        createTexture.destroy();
    }

    // View management
    // ---------------
    function addView(view:ParticleView) {
        views.push(view);
    }
    function removeView(view:ParticleView) {
        views.remove(view);
    }
    inline function createViewRenderParameters()
        return graphicsDevice.createTechniqueParameters(Reflect.copy(renderParameters));
    inline function createRenderableDrawParameters(pass) {
        var p = graphicsDevice.createDrawParameters();
        p.setVertexBuffer(0, particleVertices);
        p.setSemantics   (0, particleSemantics);
        p.technique = renderer.technique;
        p.primitive = graphicsDevice.PRIMITIVE_TRIANGLES;
        p.count     = numParticleVertices;
        p.userData  = { passIndex: pass };
        return p;
    }

    // Mapping table sorting.
    // ----------------------
    function sortMappingTable(view:ParticleView) {
        // skip sorting if the system has no live particles.
        if (!hasLiveParticles) return;

        // Ensure internal techniques are created.
        initSortTechniques(graphicsDevice);

        graphicsDevice.setStream(textureVertices, textureSemantics);
        prepareForSort(view);
        graphicsDevice.setTechnique(mergeSortTechnique);
        for (i in 0...maxSortSteps)
            if (performSortPass(view)) break;
    }
    function prepareForSort(view:ParticleView) {
        // compute min/max z-bounds on system for given view.
        // we compute this as the min/max projections of the extent corners
        // after transformation by the modelview onto the z-axis.
        //
        // We ignore modelview translation for this computation, similarly ignored
        // in the shader, avoiding unnecessary computation and similarly ignore
        // extents center and halfExtents which have no effect on the sorting.
        var abs = Math.abs;
        var mv  = view.modelView;
        var parameters = prepareSortParameters;
        parameters.mappingTable  = view.mappingTextures[view.current];
        parameters.particleState = context.textures[current];
        parameters.modelView     = view.modelView;
        parameters.zBound        = abs(mv.m13) + abs(mv.m23) + abs(mv.m33);
        renderToTarget(parameters, prepareSortTechnique, view.mappingTargets[1 - view.current]);
        view.current = 1 - view.current;
    }
    function performSortPass(view:ParticleView) {
        var pass  = (1 << view.mergePass);
        var stage = (1 << view.mergeStage);

        var parameters = mergeSortParameters;
        parameters.cpass          = pass;
        parameters.PmS            = (pass % stage);
        parameters.twoStage       = (2 * stage);
        parameters.twoStage_PmS_1 = (2 * stage) - (pass % stage) - 1;
        parameters.mappingTable   = view.mappingTextures[view.current];
        renderToTarget(parameters, null, view.mappingTargets[1 - view.current]);
        view.current = 1 - view.current;

        view.mergePass--;
        if (view.mergePass < 0) {
            view.mergeStage++;
            view.mergePass = view.mergeStage;
            if (view.mergeStage > maxMergeStage) {
                view.mergePass = view.mergeStage = 0;
                return true;
            }
        }
        return false;
    }

    // System rendering
    // ----------------
    inline function renderToTarget(parameters, technique, target) {
        graphicsDevice.beginRenderTarget(target);
        if (technique != null) graphicsDevice.setTechnique(technique);
        graphicsDevice.setTechniqueParameters(parameters);
        graphicsDevice.draw(graphicsDevice.PRIMITIVE_TRIANGLE_STRIP, 4, 0);
        graphicsDevice.endRenderTarget();
    }

    function render(view:ParticleView) {
        // skip rendering if the system has no live particles.
        if (!hasLiveParticles) return;

        graphicsDevice.setStream(particleVertices, particleSemantics);
        graphicsDevice.setTechnique(renderer.technique);
        graphicsDevice.setTechniqueParameters(updateRenderParameters(renderParameters, view));
        graphicsDevice.draw(graphicsDevice.PRIMITIVE_TRIANGLES, numParticleVertices, 0);
    }
    function updateRenderParameters(renderParameters:TechniqueParameters, view:ParticleView) {
        renderParameters.vparticleState = context.textures[current];
        renderParameters.fparticleState = context.textures[current];
        if (zSorted)
            renderParameters.mappingTable = view.mappingTextures[view.current];
        renderParameters.modelView  = view.modelView;
        renderParameters.projection = view.projection;
        return renderParameters;
    }

    public function renderDebug() {
        // Ensure internal techniques are created.
        initDebugTechniques(graphicsDevice);

        var tex = context.textures[current];
        var uv  = context.uvRect;
        var rs  = [uv.z - uv.x, uv.w - uv.y];
        var width  = 0.35 * graphicsDevice.height / graphicsDevice.width;
        var height = 0.35 * rs[1] / rs[0];

        graphicsDevice.setStream(textureVertices, textureSemantics);
        graphicsDevice.setTechnique(debugParticlesTechnique);

        debugParticlesParameters.textureSize    = [tex.width, tex.height];
        debugParticlesParameters.invTextureSize = [1 / tex.width, 1 / tex.height];
        debugParticlesParameters.regionSize     = rs;
        debugParticlesParameters.invRegionSize  = [1 / rs[0], 1 / rs[1]];
        debugParticlesParameters.regionPos      = [uv.x, uv.y];
        debugParticlesParameters.texture        = tex;
        debugParticlesParameters.renderQuad     = [-1, 1-height, width, height];
        graphicsDevice.setTechniqueParameters(debugParticlesParameters);
        graphicsDevice.draw(graphicsDevice.PRIMITIVE_TRIANGLE_STRIP, 4, 0);

        var i = 1;
        if (zSorted)
        {
            graphicsDevice.setTechnique(debugMappingTechnique);
            for (view in views) {
                debugMappingParameters.texture        = view.mappingTextures[view.current];
                debugMappingParameters.renderQuad     = [-1 + i*width, 1-height, width, height];
                graphicsDevice.setTechniqueParameters(debugMappingParameters);
                graphicsDevice.draw(graphicsDevice.PRIMITIVE_TRIANGLE_STRIP, 4, 0);
                i++;
            }
            graphicsDevice.setTechnique(debugParticlesTechnique);
        }

        debugParticlesParameters.texture    = createTexture;
        debugParticlesParameters.renderQuad = [-1 + i*width, 1-height, width, height];
        graphicsDevice.setTechniqueParameters(debugParticlesParameters);
        graphicsDevice.draw(graphicsDevice.PRIMITIVE_TRIANGLE_STRIP, 4, 0);
    }

    // Particle Management
    // -------------------
    public function createParticle(
        position:Vector3,
        velocity:Vector3,
        lifeTime:Float,
        animationRange:Vector2,
        ?userData:Int=0,
        ?forceCreation:Bool=false,
        ?isTracked:Bool=false
    ):Null<ParticleID> {
        if (lifeTime <= 0) return null;
        if (lifeTime > maxLifeTime) lifeTime = maxLifeTime;

        var id = queue.create(lifeTime, forceCreation);
        if (id == null) return null;

        var normalizedLife = lifeTime / maxLifeTime;
        var anim = animationRange;
        if (isTracked && trackingEnabled) {
            if (queue.wasForced) {
                // If particle creation was forced, then we may already track this particle id
                //    and must check. Sadly this is linear. TODO; is there a reasonable way
                //    to improve the efficiency here?
                var found = false;
                for (index in 0...numTracked)
                    if (tracked[index] == id) {
                        found = true;
                        break;
                    }
                if (!found) addTracked(id);
            }
            else addTracked(id);

            var index = id * PARTICLE_SPAN;
            cpu[index + PARTICLE_POS+0] = clamp(-1,1, (position.x - center.x) * invHExtents.x);
            cpu[index + PARTICLE_POS+1] = clamp(-1,1, (position.y - center.y) * invHExtents.y);
            cpu[index + PARTICLE_POS+2] = clamp(-1,1, (position.z - center.z) * invHExtents.z);

            cpu[index + PARTICLE_VEL+0] = clamp(-1,1, velocity.x * invHExtents.x);
            cpu[index + PARTICLE_VEL+1] = clamp(-1,1, velocity.y * invHExtents.y);
            cpu[index + PARTICLE_VEL+2] = clamp(-1,1, velocity.z * invHExtents.z);

            cpuI[index + PARTICLE_LIFE] = Encode.encodeUnsignedFloat2xy(normalizedLife, normalizedLife);
            cpuI[index + PARTICLE_ANIM] = Encode.encodeUnsignedFloat2xy(anim.y - anim.x, anim.y);
            cpuI[index + PARTICLE_DATA] = userData;
        }

        // Determine index into creation texture.
        var u = (id % particleSize.x) * PARTICLE_DIM;
        var v = Std.int((id - u / PARTICLE_DIM) / particleSize.x) * PARTICLE_DIM;

        var w = particleSize.x * PARTICLE_DIM;
        var index = (v * w) + u;
        createData32[index + w*0 + 0] = Encode.encodeSignedFloat((position.x - center.x) * invHExtents.x);
        createData32[index + w*1 + 0] = Encode.encodeSignedFloat((position.y - center.y) * invHExtents.y);
        createData32[index + w*2 + 0] = Encode.encodeSignedFloat((position.z - center.z) * invHExtents.z);
        createData32[index + w*0 + 1] = Encode.encodeSignedFloat(velocity.x * invHExtents.x);
        createData32[index + w*1 + 1] = Encode.encodeSignedFloat(velocity.y * invHExtents.y);
        createData32[index + w*2 + 1] = Encode.encodeSignedFloat(velocity.z * invHExtents.z);
        createData32[index + w*0 + 2] = Encode.encodeUnsignedFloat2xy(normalizedLife, normalizedLife);
        createData32[index + w*1 + 2] = Encode.encodeUnsignedFloat2xy(anim.y - anim.x, anim.y);
        createData32[index + w*2 + 2] = userData;
        addCreated(id);

        return id;
    }
    public function removeParticle(particleID:ParticleID) {
        if (!trackingEnabled)
            return;

        queue.removeParticle(particleID);

        // Remove from tracked list, shuffle back anything after.
        var j = 0;
        for (i in 0...numTracked)
            if (tracked[i] != particleID)
                tracked[j++] = tracked[i];
        numTracked--;

        // 'Create' a dead particle to replace particleID's particle.
        var u = (particleID % particleSize.x) * PARTICLE_DIM;
        var v = Std.int((particleID - u / PARTICLE_DIM) / particleSize.x) * PARTICLE_DIM;

        // TotalLife is used in shader to check if a new particle exists
        // In this case we want to 'purge' an existing particle, so
        // we set life = 0, totalLife <> 0 to overwrite particle and make it
        // dead. We don't care about the other attributes.
        var w = particleSize.x * PARTICLE_DIM;
        var index = (v * w) + u;
        createData32[index + w*0 + 2] = 0x0000ffff;
        addCreated(particleID);
    }
    public function removeAllParticles() {
        if (trackingEnabled)
            numTracked = 0;

        queue.clear();
        var w = PARTICLE_DIM;
        // (As per removeParticle)
        for (i in 0...particleSize.x)
        for (j in 0...particleSize.y) {
            createData32[(w * w * particleSize.x * j) + (w * i) + w*0 + 2] = 0x0000ffff;
            addCreated(j * particleSize.x + i);
        }
    }
    public function updateParticle(
        particleID:ParticleID,
        ?position:Vector3,
        ?velocity:Vector3,
        ?lifeTime:Float,
        ?animationRange:Vector2,
        ?userData:Int,
        ?isTracked:Bool = true
    ) {
        if (!trackingEnabled)
            return;

        var index = particleID * PARTICLE_SPAN;
        if (lifeTime != null && lifeTime > maxLifeTime) lifeTime = maxLifeTime;

        // Update queue for particle life-time change.
        var lifeDelta =
            if (lifeTime == null) null
            else (lifeTime - maxLifeTime * Encode.decodeHalfUnsignedFloat(cpuI[index + PARTICLE_LIFE] >>> 16));
        if (lifeDelta != null && lifeDelta != 0.0)
            queue.updateParticle(particleID, lifeDelta);

        // normalize lifeTime
        if (lifeTime != null)
            lifeTime /= maxLifeTime;

        // Update CPU side data for particle.
        if (position != null) {
            cpu[index + PARTICLE_POS+0] = clamp(-1,1, (position.x - center.x) * invHExtents.x);
            cpu[index + PARTICLE_POS+1] = clamp(-1,1, (position.y - center.y) * invHExtents.y);
            cpu[index + PARTICLE_POS+2] = clamp(-1,1, (position.z - center.z) * invHExtents.z);
        }
        if (velocity != null) {
            cpu[index + PARTICLE_VEL+0] = clamp(-1,1, velocity.x * invHExtents.x);
            cpu[index + PARTICLE_VEL+1] = clamp(-1,1, velocity.y * invHExtents.y);
            cpu[index + PARTICLE_VEL+2] = clamp(-1,1, velocity.z * invHExtents.z);
        }
        if (lifeTime != null) {
            var c = Encode.encodeHalfUnsignedFloat(lifeTime);
            cpuI[index + PARTICLE_LIFE] = (c << 16) | (cpuI[index + PARTICLE_LIFE] & 0xffff);
        }
        var anim = animationRange;
        if (anim != null)
            cpuI[index + PARTICLE_ANIM] = Encode.encodeUnsignedFloat2xy(anim.y - anim.x, anim.y);
        if (userData != null)
            cpuI[index + PARTICLE_DATA] = userData;

        // Remove from tracked list if no longer want particle to be tracked on CPU.
        if (!isTracked) {
            // Shuffle back anything after.
            var j = 0;
            for (i in 0...numTracked)
                if (tracked[i] != particleID)
                    tracked[j++] = tracked[i];
            numTracked--;
        }

        var u = (particleID % particleSize.x) * PARTICLE_DIM;
        var v = Std.int((particleID - u / PARTICLE_DIM) / particleSize.x) * PARTICLE_DIM;

        var w = particleSize.x * PARTICLE_DIM;
        var i = (v * w) + u;
        var data = createData32;
        data[i + w*0 + 0] = Encode.encodeSignedFloat(cpu[index + PARTICLE_POS+0]);
        data[i + w*1 + 0] = Encode.encodeSignedFloat(cpu[index + PARTICLE_POS+1]);
        data[i + w*2 + 0] = Encode.encodeSignedFloat(cpu[index + PARTICLE_POS+2]);
        data[i + w*0 + 1] = Encode.encodeSignedFloat(cpu[index + PARTICLE_VEL+0]);
        data[i + w*1 + 1] = Encode.encodeSignedFloat(cpu[index + PARTICLE_VEL+1]);
        data[i + w*2 + 1] = Encode.encodeSignedFloat(cpu[index + PARTICLE_VEL+2]);
        data[i + w*0 + 2] = cpuI[index + PARTICLE_LIFE];
        data[i + w*1 + 2] = cpuI[index + PARTICLE_ANIM];
        data[i + w*2 + 2] = cpuI[index + PARTICLE_DATA];
        addCreated(particleID);
    }

    function addCreated(id:Int) {
        // We don't worry about duplicates here, the amout of work done to clear the same
        //   particle twice in stupidly rare circumstances is less than checking it doesn't
        //   occur in the first place.
        var total = numCreated + 1;
        if (total >= createdIndex.length) {
            // Resize array
            var size = createdIndex.length;
            while (size < total) size = Std.int(size * 1.5);

            // Copy old data to new array.
            var newData = new Uint16Array(size);
            for (i in 0...numCreated) newData[i] = createdIndex[i];
            createdIndex = newData;
        }

        // We store the particle ID (which is at most 65535) so that we can use an
        // UInt16 array for this list, otherwise storing 'index' would require a UInt32 array.
        createdIndex[numCreated] = id;
        numCreated++;
    }
    function addTracked(id:Int) {
        var total = numTracked + 1;
        if (total >= tracked.length) {
            // Resize array, ensuring we don't make it any longer than can ever be necessary.
            var size = tracked.length;
            while (size < total) size = Std.int(size * 1.5);
            if (size > maxParticles) size = maxParticles;

            // Copy old data to new array.
            var newData = new Uint16Array(size);
            for (i in 0...numTracked) newData[i] = tracked[i];
            tracked = newData;
        }
        tracked[numTracked] = id;
        numTracked++;
    }

    public function queryPosition(trackedParticle:ParticleID, ?dst:Vector3):Vector3 {
        if (!trackingEnabled) return null;
        if (dst == null) dst = new Float32Array(3);
        dst[0] = cpu[trackedParticle*PARTICLE_SPAN + PARTICLE_POS+0] * halfExtents.x + center.x;
        dst[1] = cpu[trackedParticle*PARTICLE_SPAN + PARTICLE_POS+1] * halfExtents.y + center.y;
        dst[2] = cpu[trackedParticle*PARTICLE_SPAN + PARTICLE_POS+2] * halfExtents.z + center.z;
        return dst;
    }
    public function queryVelocity(trackedParticle:ParticleID, ?dst:Vector3):Vector3 {
        if (!trackingEnabled) return null;
        if (dst == null) dst = new Float32Array(3);
        dst[0] = cpu[trackedParticle*PARTICLE_SPAN + PARTICLE_VEL+0] * halfExtents.x;
        dst[1] = cpu[trackedParticle*PARTICLE_SPAN + PARTICLE_VEL+1] * halfExtents.y;
        dst[2] = cpu[trackedParticle*PARTICLE_SPAN + PARTICLE_VEL+2] * halfExtents.z;
        return dst;
    }
    // Returns true remaining lifeTime (not normalised)
    public inline function queryLife(trackedParticle:ParticleID):Float {
        if (!trackingEnabled) return null;
        var pix = cpuI[trackedParticle*PARTICLE_SPAN + PARTICLE_LIFE];
        return Encode.decodeHalfUnsignedFloat(pix>>>16) * maxLifeTime;
    }
    // Returns normalised, ranged animation time in [0,1]
    // If particle is created with range [0.5, 0.7], value will be between 0.5 and 0.7
    public function queryAnimationTime(trackedParticle:ParticleID):Float {
        if (!trackingEnabled) return null;
        var pix   = cpuI[trackedParticle*PARTICLE_SPAN + PARTICLE_LIFE];
        var c01   = cpuI[trackedParticle*PARTICLE_SPAN + PARTICLE_ANIM];
        var life  = Encode.decodeHalfUnsignedFloat(pix >>> 16);
        var total = Encode.decodeHalfUnsignedFloat(pix & 0xffff);
        var c0    = Encode.decodeHalfUnsignedFloat(c01 >>> 16);
        var c1    = Encode.decodeHalfUnsignedFloat(c01 & 0xffff);
        return c1 - (life / total) * c0;
    }
    public inline function queryData(trackedParticle:ParticleID):Int {
        if (!trackingEnabled) return null;
        return cpuI[trackedParticle * PARTICLE_SPAN + PARTICLE_DATA];
    }

    // System update
    // -------------
    function sync(frameVisible:Int, time:Float) {
        if (lastVisible == null) lastVisible = frameVisible - 1;
        if (frameVisible != lastVisible) {
            if (synchronize != null)
                synchronize(this, frameVisible - lastVisible, time - lastTime);
            lastVisible = frameVisible;
            lastTime = time;
        }
    }
    public function reset(time:Float) {
        // TODO reset particles too, not presently needed.
        // as presently, only call this on empty systems being recycled.
        lastVisible = null;
        lastTime = time;
    }

    // Update the creation queue to free up space for particles
    // that will die during the time step.
    //
    // This is used by emitters to perform the clean up
    // before generating new particles in their place and updating
    // the state of the system seperately.
    var shouldUpdate:Bool;
    var hasLiveParticles = false;
    public function prune(timeStep:Float) {
        shouldUpdate = hasLiveParticles;
        hasLiveParticles = queue.update(timeStep);
    }
    public function step(timeStep:Float, ?shift:Vector3) {
        if (shouldUpdate || numCreated != 0)
            updateParticleState(timeStep, shift);
        hasLiveParticles = hasLiveParticles || (numCreated != 0);
        return hasLiveParticles;
    }

    function dispatchBuffer() {
        if (numCreated != 0 || createForceFlush) {
            createForceFlush = false;
            createTexture.setData(createData);
        }

        if (numCreated == 0) return;

        var w = particleSize.x * PARTICLE_DIM;
        for (i in 0...numCreated) {
            var j = createdIndex[i];

            // Map UInt16 index back into a full index.
            var u = (j % particleSize.x) * PARTICLE_DIM;
            var v = Std.int((j - u / PARTICLE_DIM) / particleSize.x) * PARTICLE_DIM;
            var j = (v * w) + u;

            createData32[j + w*0 + 0] = createData32[j + w*0 + 1] = createData32[j + w*0 + 2] =
            createData32[j + w*1 + 0] = createData32[j + w*1 + 1] = createData32[j + w*1 + 2] =
            createData32[j + w*2 + 0] = createData32[j + w*2 + 1] = createData32[j + w*2 + 2] = 0;
        }

        createForceFlush = true;
        numCreated = 0;
    }
    function updateParticleState(timeStep:Float, shift:Null<Vector3>) {
        dispatchBuffer();

        var lifeStep = updateParameters.lifeStep = timeStep * 1/maxLifeTime;
        var timeStep = updateParameters.timeStep = timeStep;

        var uShift:Vector3 = updateParameters.shift;
        if (shift == null) {
            // set to 0.
            uShift.x = uShift.y = uShift.z = 0;
        } else {
            // Scale by invHExtents
            uShift.x = shift.x * invHExtents.x;
            uShift.y = shift.y * invHExtents.y;
            uShift.z = shift.z * invHExtents.z;
        }

        graphicsDevice.setStream(textureVertices, textureSemantics);
        updateParameters.previousState = context.textures[current];
        renderToTarget(updateParameters, updater.technique, context.targets[1 - current]);
        current = 1 - current;

        // tracked particles on CPU
        numTracked = updater.update(cpu, cpuI, tracked, numTracked, updateParameters);
    }
}
