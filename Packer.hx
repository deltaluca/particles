// Best fit decreasing.
// Thanks to jakesgordon
// https://github.com/jakesgordon/bin-packing

typedef Size = {
    w:Int,
    h:Int,
};

typedef Pos = {
    x:Int,
    y:Int
};

typedef Node = {
    x:Int, y:Int,
    w:Int, h:Int,
    used:Bool,
    down:Null<Node>,
    right:Null<Node>
};

class Packer {
    static var root:Null<Node> = null;
    public static function pack(
        sizes:Array<Size>
    ):{
        packing:Map<Int, Pos>,
        size:Size
    } {
        if (sizes.length == 0) return { packing: new Map(), size: {w:0, h:0} };

        // Sort element ids by box size. (Simple brute force.)
        var processed = new Map();
        var backRef = [];
        for (i in 0...sizes.length) {
            var max = -1;
            var maxSize = 0;
            for (j in 0...sizes.length) {
                if (processed[j]) continue;
                var box = sizes[j];
                var size = box.w > box.h ? box.w : box.h;
                if (size > maxSize || max == -1) {
                    max = j;
                    maxSize = size;
                }
            }
            backRef.push(max);
            processed[max] = true;
        }

        var packing = new Map();
        root = node(0, 0, sizes[backRef[0]].w, sizes[backRef[0]].h);
        for (i in 0...sizes.length) {
            var block = sizes[backRef[i]];
            var w = block.w;
            var h = block.h;
            var node = find(root, w, h);
            var fit = if (node != null) split(node, w, h) else grow(w, h);
            packing[backRef[i]] = { x: fit.x, y: fit.y };
        }

        var ret = {
            packing: packing,
            size: { w: root.w, h: root.h }
        };

        root = null; // leave to GC
        return ret;
    }

    static inline function node(x, y, w, h)
        return {
            x: x, y: y,
            w: w, h: h,
            used: false,
            down: null,
            right: null
        };

    static function find(root:Node, w, h) {
        if (root.used) {
            var a = find(root.down, w, h);
            if (a == null) a = find(root.right, w, h);
            return a;
        }
        else if (w <= root.w && h <= root.h)
             return root;
        else return null;
    }

    static function split(root:Node, w, h) {
        root.used  = true;
        root.down  = node(root.x, root.y + h, root.w, root.h - h);
        root.right = node(root.x + w, root.y, root.w - w, h);
        return root;
    }

    static function grow(w, h) {
        var canGrowDown  = w <= root.w;
        var canGrowRight = h <= root.h;

        // Try and keep packed size square-like
        var shouldGrowRight = canGrowRight && (root.h >= (root.w + w));
        var shouldGrowDown  = canGrowDown  && (root.w >= (root.h + h));

        return if (shouldGrowRight) growRight(w,h);
          else if (shouldGrowDown)  growDown (w,h);
          else if (canGrowRight)    growRight(w,h);
          else if (canGrowDown)     growDown (w,h);
          else                      null;
    }

    static function growRight(w, h) {
        var newRoot   = node(0, 0, root.w + w, root.h);
        newRoot.used  = true;
        newRoot.down  = root;
        newRoot.right = node(root.w, 0, w, root.h);
        root = newRoot;
        var node = find(root, w, h);
        return if (node != null) split(node, w, h) else null;
    }

    static function growDown(w, h) {
        var newRoot   = node(0, 0, root.w, root.h + h);
        newRoot.used  = true;
        newRoot.down  = node(0, root.h, root.w, h);
        newRoot.right = root;
        root = newRoot;
        var node = find(root, w, h);
        return if (node != null) split(node, w, h) else null;
    }
}
